var objData = "";
var period = "";
var getPeriods = $.get( base_url+"home/getPeriods", function(data) {
    objData = JSON.parse(data);
    if(objData.status){
        $("#selectPeriod").html('');
        $.each(objData.data, function(key,value) {
            $("#selectPeriod").append(`<option value="${value.idPeriod_int}">${value.periodName}</option>`);
        });
        if(period = localStorage.getItem('period')){
            $(`#selectPeriod option[value="${period}"]`).prop('selected', true);
            filterData(period);
        }
    }
}).fail(function() {
    alert( "Error al cargar periodos" );
}).always(function() {
    
});

/* FILTRA CAMPUS POR PERIODO SELECCIONADO */
$('#btn_filter').on('click', function(){
    idPeriod = $("#selectPeriod").val();
    localStorage.setItem('period', idPeriod);
    filterData(idPeriod);
});
const filterData = (idPeriod) => {
    getData_Date(idPeriod);
    get_MainData(idPeriod);
    getResourceByChallenge(idPeriod);
    getResourceByGrade(idPeriod);
}
/* MAIN DATA */
var obj_MainData = "";
var json_MainData;
const get_MainData = (idPeriod_int) => {
    $.get( base_url+"home/get_MainData?idPeriod_int="+idPeriod_int, function(data) {
        objData = JSON.parse(data);
        $("#version").html(objData[0]);
        $("#tot_recurosos").html(objData[1]);
        $("#active_campus").html(objData[2].detonedCampus);
        $("#tot_campus").html("De "+objData[2].totCampus+" campus");
        $("#tot_alumnos").html(objData[3]);
    }).fail(function() {
        alert("Error al cargar Main data");
    }).always(function() {
        
    });
}
/* GRAFICA LINEAL */
var json_data;
var labelChart = [];
var dataChart = [];
let chart_Dates;
const getData_Date = (idPeriod_int) => {
    var jqxhr = $.get( base_url+"home/getData_Date?idPeriod_int="+idPeriod_int, function(data) {
        json_data = JSON.parse(data);
        if(chart_Dates) chart_Dates.destroy();
        if(json_data.status){
            labelChart = [];
            dataChart = [];
            for (let index = 0; index < json_data.data.length; index++) {
                labelChart.push(json_data.data[index].fecha);
                dataChart.push(json_data.data[index].total);
            }
            const infoChart = {
                labels: labelChart,
                datasets: [{
                    label: 'Total de registros diarios',
                    backgroundColor: '#26c6da',
                    borderColor: '#26c6da',
                    data: dataChart,
                    pointRadius: 2
                }]
            };
            const configChart = {
                type: 'line',
                data: infoChart,
                options: {
                    responsive:true,
                    maintainAspectRatio: false,
                    scales:{
                        x:{
                            display:true
                        },
                        y:{
                            display:true
                        }
                    },
                    plugins:{
                        legend:{
                            display:true
                        }
                    }
                }
            };
            chart_Dates = new Chart(
                document.getElementById('dateResourceChart'),
                configChart
            );
            chart_Dates.height = 500;
        }
        else{
            alert(json_data.msg);
        }
    }).fail(function() {
        alert("Error al cargar datos por fecha");
    });
}
var json_dataChallenge;
var labelChartChallenge = [];
var dataChartChallenge = [];
let chart_DatesChallenge;
const getResourceByChallenge = (idPeriod_int) => {
    var jqxhr = $.get( base_url+"home/get_graphicDataChallenge?idPeriod_int="+idPeriod_int, function(data) {
        json_dataChallenge = JSON.parse(data);
        if(chart_DatesChallenge) chart_DatesChallenge.destroy();

        if(json_dataChallenge.status){
            labelChartChallenge = [];
            dataChartChallenge = [];
            for (let index = 0; index < json_dataChallenge.data.length; index++) {
                labelChartChallenge.push("Reto "+json_dataChallenge.data[index].challengeNumber);
                dataChartChallenge.push(json_dataChallenge.data[index].Percent*100);
            }
            const infoChart = {
                labels: labelChartChallenge,
                datasets: [{
                    label: 'Porcentaje de uso',
                    backgroundColor: '#26c6da',
                    borderColor: '#26c6da',
                    data: dataChartChallenge,
                    pointRadius: 2
                }]
            };
            const configChart = {
                type: 'bar',
                data: infoChart,
                options: {
                    scales:{
                        x:{
                            display:true
                        },
                        y:{
                            max: 100,
                            min: 0,
                            ticks: {
                                stepSize: 10
                            }
                        }
                    },
                    plugins:{
                        legend:{
                            display:true
                        }
                    }
                }
            };
            chart_DatesChallenge = new Chart(
                document.getElementById('challengeResourcesChart'),
                configChart
            );
            // chart_Dates.height = 500;
        }
        else{
            alert(json_dataChallenge.msg);
        }
    }).fail(function() {
        alert("Error al cargar recursos por reto");
    });
}
var json_dataGrade;
var labelChartGrade = [];
var dataChartGrade = [];
let chart_DatesGrade;
const getResourceByGrade = (idPeriod_int) => {
    var jqxhr = $.get( base_url+"home/get_graphicDataGrade?idPeriod_int="+idPeriod_int, function(data) {
        json_dataGrade = JSON.parse(data);
        if(chart_DatesGrade) chart_DatesGrade.destroy();

        if(json_dataGrade.status){
            labelChartGrade = [];
            dataChartGrade = [];
            for (let index = 0; index < json_dataGrade.data.length; index++) {
                labelChartGrade.push(json_dataGrade.data[index].initialsGrade);
                dataChartGrade.push(json_dataGrade.data[index].Percent*100);
            }
            const infoChart = {
                labels: labelChartGrade,
                datasets: [{
                    label: 'Porcentaje de uso',
                    backgroundColor: '#26c6da',
                    borderColor: '#26c6da',
                    data: dataChartGrade,
                    pointRadius: 2
                }]
            };
            const configChart = {
                type: 'bar',
                data: infoChart,
                options: {
                    scales:{
                        x:{
                            display:true
                        },
                        y:{
                            max: 100,
                            min: 0,
                            ticks: {
                                stepSize: 10
                            }
                        }
                    },
                    plugins:{
                        legend:{
                            display:true
                        }
                    }
                }
            };
            chart_DatesGrade = new Chart(
                document.getElementById('gradeResourcesChart'),
                configChart
            );
            // chart_Dates.height = 500;
        }
        else{
            alert(json_dataChallenge.msg);
        }
    }).fail(function() {
        alert("Error al cargar recursos por grado");
    });
}