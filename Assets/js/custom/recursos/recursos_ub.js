var objDataPeriod = "";
var period;
var challenge;
var dataLoaded = false;
var jqxhr = $.get( base_url+"recursos/getPeriod", function(data) {
    objDataPeriod = JSON.parse(data);
    if(objDataPeriod.status){
        $.each(objDataPeriod.data, function(key,value) {
            $("#selectPeriod").append(`<option value="${value.idPeriod_int}">${value.name}</option>`);
        });
        if(period = localStorage.getItem('period')){
            $(`#selectPeriod option[value="${period}"]`).prop('selected', true);
            if(!dataLoaded) getChallenge(period);
        }
    }
    else{
        alert(objDataChallenge.msg);
    }
}).fail(function() {
    $("#selectPeriod").html(`<option value="error">Error al cargar filtros</option>`);
});

$("#selectPeriod").change(function(){
    var objDataChallenge = "";
    var value = $("#selectPeriod").val();
    getChallenge(value);
});

$("#filterResource").on("click", function(){
    var objDataResource = "";
    var idPeriod = $("#selectPeriod").val();
    var Challenge = $("#selectChallenge").val();
    getResourceData(idPeriod, Challenge);
});

const getChallenge = (idPeriod) => {
    var jqxhr = $.get( base_url+"recursos/getChallengePeriod?idPeriod_int="+idPeriod, function(data) {
        objDataChallenge = JSON.parse(data);
        if(objDataChallenge.status){
            $("#selectChallenge").html('');
            $.each(objDataChallenge.data, function(key,value) {
                $("#selectChallenge").append(`<option value="${value.number}">Reto ${value.number}</option>`);
            });
            if(challenge = localStorage.getItem('challenge')){
                $(`#selectChallenge option[value="${challenge}"]`).prop('selected', true);
                if(!dataLoaded) getResourceData(localStorage.getItem('period'), challenge);
            }
        }
        else{
            alert(objDataChallenge.msg);
        }
    }).fail(function() {
        $("#selectChallenge").html(`<option value="error">Error al cargar filtros</option>`);
    });
}
const getResourceData = (idPeriod, idChallenge) =>{
    $("#resourcesTable tbody").html('');
    $('.grades h3').text('-');
    var jqxhr = $.get( base_url+"recursos/getResource_ub?idPeriod_int="+idPeriod+"&challengeNumber="+idChallenge, function(data) {
        objDataResource = JSON.parse(data);
        if(objDataResource.status){
            localStorage.setItem('period', idPeriod);
            localStorage.setItem('challenge', idChallenge);
            $("#titleResource").text($("#selectPeriod option:selected").text());
            $("#subtitleResource").text($("#selectChallenge option:selected").text());
            resourceTable.clear();
            $.each(objDataResource.data, function(key,value) {
                if($('.'+value.initials+' h3').text() != '-'){
                    $('.'+value.initials+' h3').text(parseInt($('.'+value.initials+' h3').text())+1);
                }
                else{
                    $('.'+value.initials+' h3').text(1);
                }
                resourceTable.row.add([
                    key+1,
                    value.initials,
                    value.title,
                    "ver"
                ]);
            });
            resourceTable.draw();
            dataLoaded = true;
        }
        else{
            resourceTable.clear().draw();
        }
    }).fail(function() {
        $("#selectChallenge").html(`<option value="error">Error al cargar filtros</option>`);
    });
}
$.fn.dataTable.ext.search.push(function (settings, data, dataIndex) {
    var GradeFilter = 'E1';
    var GradeTable = parseFloat(data[2]) || 0; // use data for the age column
    console.log(GradeFilter,GradeTable);
    if (GradeFilter == GradeTable){
        return true;
    }
    return false;
});

/*
'1','8','E5','1','105234','313501','e5_es_r1_dispObsoletos'
'1','8','E5','1','169874','413510','v4_e5_es_r1_elCicloDeLaVida'
'1','8','E5','1','204653','513506','v5_e5_es_r1_aQueLugarLlegaste_MA'
'1','8','E5','1','204654','513512','v5_e5_en_r1_whatPlaceDidYouGetTo_MA'*
'1','8','E5','1','204724','513501','v5_e5_es_r1_17'
'1','8','E5','1','204726','513502','v5_e5_es_r1_miEleccionDePlatillo'*
'1','8','E5','1','205553','513503','v5_e5_es_r1_aparatoDigestivo'*
'1','8','E5','1','214439','613514','v6_e5_es_r1_17'





'1','MX 2022 - 2023','8','E5','1','3196','111371','¡Dispositivos obsoletos!','Technology','6','1','1','immersive://action=openActivity&description=Interactive&activity=313501&userId={{userid}}&username={{username}}&password={{password}}&reto={{reto}}&grado={{grado}}'
*'1','MX 2022 - 2023','8','E5','1','3196','111373','Healthy Eating','English','14','100','3','immersive://action=openActivity&description=Interactive&activity=413506&userId={{userid}}&username={{username}}&password={{password}}&reto={{reto}}&grado={{grado}}'
*'1','MX 2022 - 2023','8','E5','1','3196','111778','The Endocrine System','English','6','100','2','immersive://action=openActivity&description=Interactive&activity=413505&userId={{userid}}&username={{username}}&password={{password}}&reto={{reto}}&grado={{grado}}'
*'1','MX 2022 - 2023','8','E5','1','3196','112080','What\'s the Point of View?','English','16','300','1','immersive://action=openActivity&description=Interactive&activity=413507&userId={{userid}}&username={{username}}&password={{password}}&reto={{reto}}&grado={{grado}}'
'1','MX 2022 - 2023','8','E5','1','3196','168633','El ciclo de la vida','Español','1','100','3','immersive://action=openActivity&description=Interactive&activity=413510&userId={{userid}}&username={{username}}&password={{password}}&reto={{reto}}&grado={{grado}}'
'1','MX 2022 - 2023','8','E5','1','3196','168660','¡Mi elección de platillo!','Español','3','100','2','immersive://action=openActivity&description=Interactive&activity=513502&userId={{userid}}&username={{username}}&password={{password}}&reto={{reto}}&grado={{grado}}'
'1','MX 2022 - 2023','8','E5','1','3196','177150','Kn·saving the Planet Initial Trigger','English','16','1000','3',NULL 
'1','MX 2022 - 2023','8','E5','1','3196','177152','Let\'s Save the Planet Triggers','English','16','1000','3',NULL
'1','MX 2022 - 2023','8','E5','1','3196','205652','¿A qué lugar llegaste?','Español','13','400','2','immersive://action=openActivity&description=Interactive&activity=513506&userId={{userid}}&username={{username}}&password={{password}}&reto={{reto}}&grado={{grado}}'
*'1','MX 2022 - 2023','8','E5','1','3196','206783','Kn·saving the Planet 2','English','17','900','3','immersive://action=openActivity&description=Interactive&activity=514001&userId={{userid}}&username={{username}}&password={{password}}&reto={{reto}}&grado={{grado}}'
*/