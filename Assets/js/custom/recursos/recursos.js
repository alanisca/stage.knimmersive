let findForm = document.getElementById("buscarForm");
findForm.addEventListener("submit", (e) =>{
    e.preventDefault();
    let Period = document.getElementById("period").value;
    let challenge = document.getElementById("challenge").value;
    let grade = document.getElementById("grade").value;

    resourceTable.clear();
    resourceTable.draw();
    var jqxhr = $.get( base_url+`recursos/getResource?idPeriod_int=${Period}&challengenumber=${challenge}&gradeinitials=${grade}`, function(data) {
        objDataPeriod = JSON.parse(data);
        if(objDataPeriod.status){
            console.log(objDataPeriod.data);
            $.each(objDataPeriod.data, (key, value)=>{
                resourceTable.row.add([
                    // key+1,
                    value['Titulo recurso Asociado'],
                    value['Grado'],
                    value['Pathway'],
                    value['Sesión'],
                    value['No. Actividad'],
                    value['Paso'],
                ]);
            });
            resourceTable.draw();
        }
        else{
            alert(objDataChallenge.msg);
        }
    }).fail(function() {
        alert('Error al cargar información');
    });
});