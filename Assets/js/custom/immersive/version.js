// const getVersions = () => {
//     let formData;
//     let id_pais = $("select#pais").val();
//     console.log(id_pais);
//     var jqxhr = $.get( base_url+`immersive/getVersions?idPais_int=${id_pais}`, function(data) {
//         formData = JSON.parse(data);
//         console.log(formData);
//         if(formData.status){
//             $("#requisiciones tr.version").remove();
//             $.each(formData.data, function(key,value) {
//                 console.log(value.version,
//                     value.descripcion,
//                     value.status,
//                     value.id_version);
//                 $("#requisiciones ").append(`<tr class="version">
//                                                 <th>${value.version}</th>
//                                                 <th>${value.descripcion}</th>
//                                                 <th>${(value.status == 0 ? `<button type="button" id="${value.id_version}" class="btn btn-primary waves-effect waves-light m-r-10 cambiarVersion">Seleccionar</button>` : `Actual`)}</th>
//                                             </tr>`);
//             });
//         }
//     }).fail(function(e){
//         alert("Error al caragar :"+ e);
//     });
// }
// getVersions();
window.addEventListener('load', function(){
    if(document.querySelector("#formVersion")){
        let formVersion = document.querySelector("#formVersion");
        /* Carga nueva versión */
        formVersion.onsubmit = function(e){
            e.preventDefault();
            let strVersion = document.querySelector("#txtVersion");
            let strDescripcion = document.querySelector("#txtDescripcion");
            if(strVersion == '' || strDescripcion == ''){
                $.toast({
                    heading: 'Error: Faltan datos',
                    text: "Es necesario llenar todos los campos",
                    position: 'top-right',
                    loaderBg:'#fc4b6c',
                    icon: 'error',
                    hideAfter: 3000, 
                    stack: 1
                });
                return false;
            }
            else{
                let id_pais = $("select#pais").val();
                let request = (window.XMLHttpRequest) ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');
                let ajaxUrl = base_url+'dashboard/saveVersion?id_pais='+id_pais;
                let formData = new FormData(formVersion);
                request.open("POST",ajaxUrl,true);
                request.send(formData);
                request.onreadystatechange = function(){
                    if(request.readyState == 4 && request.status == 200){
                        console.log(request.responseText);
                        objData = JSON.parse(request.responseText);
                        if (objData.status) {
                            formVersion.reset();
                            loadVersions();
                        }
                        else{
                            $.toast({
                                heading: objData.msg,
                                text: 'Error: No se guardó la versión',
                                position: 'top-right',
                                loaderBg:'#fc4b6c',
                                icon: 'error',
                                hideAfter: 3000, 
                                stack: 1
                            });
                        }
                    }
                }
            }
        }
    }
    
    function loadVersions(){
        var id_pais = $("select#pais").val();
        var request = (window.XMLHttpRequest) ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');
        var ajaxUrl  = base_url+'dashboard/getVersions?id_pais='+id_pais;

        request.open("GET",ajaxUrl,true);
        request.send();
        // addLoad();

        request.onreadystatechange = function(){
            if(request.readyState == 4 && request.status == 200){
                // removeLoad();
                objData = JSON.parse(request.responseText);
                if(objData.status){
                    $("#requisiciones tr.version").remove();
                    $.each(objData.data, function(key,value) {
                        $("#requisiciones ").append(`<tr class="version">
                                                        <th>${value.version}</th>
                                                        <th>${value.descripcion}</th>
                                                        <th>${(value.status == 0 ? `<button type="button" id="${value.id_version}" class="btn btn-primary waves-effect waves-light m-r-10 cambiarVersion">Seleccionar</button>` : `Actual`)}</th>
                                                    </tr>`);
                    });
                }
                else{
                    $.toast({
                        heading: 'Error: Problemas con el servidor',
                        text: objData.msg,
                        position: 'top-right',
                        loaderBg:'#fc4b6c',
                        icon: 'error',
                        hideAfter: 3000, 
                        stack: 1
                    });
                }
            }
            else{
                // removeLoad();
            }
        }
    }
    loadVersions();
    $("#requisiciones").on("click",".cambiarVersion",function(){
        let idVersion = $(this).attr("id");
        var id_pais = $("select#pais").val();
        var request = (window.XMLHttpRequest) ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');
        var ajaxUrl  = base_url+'dashboard/changeVersions?v='+idVersion+'&id_pais='+id_pais;

        request.open("GET",ajaxUrl,true);
        request.send();
        request.onreadystatechange = function(){
            if(request.readyState == 4 && request.status == 200){
                objData = JSON.parse(request.responseText);
                if(objData.status){
                    formVersion.reset();
                    loadVersions();
                }
            }
        };
    });

    $("select#pais").on("change",function(){
        console.log($(this).val());
        loadVersions();
    });
});