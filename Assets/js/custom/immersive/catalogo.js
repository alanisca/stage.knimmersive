const getcatalogo = () => {
    let formData;
    var jqxhr = $.get( base_url+`immersive/getCatalogo`, function(data) {
        formData = JSON.parse(data);
        let reTypeColor;

        let unitybundleTot = 0;
        let arTot = 0;
        let vrTot = 0;
        let interactiveTot = 0;
        let prime = 0;

        if(formData.status){
            catalogoTable.clear();
            $.each(formData.data, function(key,value) {
                reTypeColor = "";
                switch (value.button_idResourceType_int) {
                    case "50":
                        reTypeColor = "label-success";
                        arTot++;
                        break;
                    case "229":
                        reTypeColor = "label-warning";
                        vrTot++;
                        break;
                    case "230":
                        reTypeColor = "label-info";
                        interactiveTot++;
                        break;
                    case "231":
                        reTypeColor = "label-primary";
                        prime++;
                        break;
                    default:
                        unitybundleTot++;
                        reTypeColor = "label-danger";
                        break;
                }
                catalogoTable.row.add([
                    `<td style="width:50px;font-size: 10px;">${value.file_unityTarget}</td>`,
                    `<td><h6>${value.file_title}</h6><small class="text-muted"><a href="https://resourcebank.knotion.com/#/recursos/ver/${value.file_idResource_int}" target="_blank">${value.file_idResource_int}</a></small></td>`,
                    `<td><h6>${(value.button_title ? value.button_title : 'Target')}</h6><small class="text-muted"> ${(value.button_idResource_int ? `<a href="https://resourcebank.knotion.com/#/realidad-aumentada/ver/${value.button_idResource_int}" target="_blank">${value.button_idResource_int}</a>` : "-")}</small></td>`,
                    `<td><span class="label ${reTypeColor}">${(value.button_ResourceType ? value.button_ResourceType : "Target")}</span></td>`,
                    `<td>VER</td>`
                ]);
            });
            $("#recursos-tot").text(arTot+vrTot+interactiveTot+prime);
            $("#targets-tot").text(-arTot-vrTot-interactiveTot-prime+unitybundleTot);
            $("#ar-tot").text(arTot);
            $("#vr-tot").text(vrTot);
            $("#interactive-tot").text(interactiveTot);
            $("#unitybundle-tot").text(unitybundleTot);
            $("#prime-tot").text(prime);
            catalogoTable.draw();
        }
        else{
            alert(formData.msg);
        }
    }).fail(function(e){
        alert("Error al caragar :"+ e);
    });
}
getcatalogo();

$('.myCheckBox').on('change', function(e){
    let searchTerms = []
    $.each($('.myCheckBox'), function(i,elem){
        if($(elem).prop('checked')){
            searchTerms.push("^" + $(this).attr('data-id') + "$")
        }
    })
    console.log(searchTerms.join('|'));
    if(searchTerms.join('|') != '')
        catalogoTable.column(3).search(searchTerms.join('|'), true, false, true).draw();
    else
        catalogoTable.column(3).search('{}').draw();
});