var campusData = "";
let idCampus_int = $("#campusName").attr("idCampus_int");
let idPeriod_int = $("#campusName").attr("idPeriod_int");
/* Recursos */
let catResources;
let catResourcesDetoned;
const getPeriods = () => {
    var period = "";
    var jqxhr = $.get( base_url+`dashboard/campusInfo?idCampus_int=${idCampus_int}&idPeriod_int=${idPeriod_int}`, function(data) {
        campusData = JSON.parse(data);
        if(campusData[0].status) getGeneralInfo(campusData[0].data[0]);
        else{alert("error");}
        if(campusData[1].status) {
            arrResourceData = campusData[1].data;
            prom_arrResourceData = campusData[5].data;
            getCampusInfo(arrResourceData, prom_arrResourceData);
        }
        else{alert("error");}
        if(campusData[2].status) {
            getVersionData(campusData[2].data);
        }
        if(campusData[3].status) {
            catResources = campusData[3].data;
        }
        if(campusData[4].status) {
            catResourcesDetoned = campusData[4].data;
        }
    }).fail(function(){
        $("#selectPeriod").html(`<option value="error">Error al cargar filtros</option>`);
    });
}
getPeriods();
const getGeneralInfo = (data) => {
    $("#campusName").html(data["campus"]);//.attr("idCampus_int",data["idCampus_int"]);
    //$("#schoolName").html(data["school"]).attr("idSchool_int",data["idSchool_int"]);
}
$("#btn_filter").on("click",function(){
    getCampusInfo(arrResourceData,prom_arrResourceData);
});
var arrRetos = [];
var arrTotalesRetos = [];
var arrGrados = [];
var arrTotalesGrados = [];
var arrResourceData = [];
var prom_arrResourceData = [];
let myChartGrade, myChartChallenge;
const getCampusInfo = (data, promData) => {
    let gradeSelect = $("#selectGrade").val();
    let challengeSelect = $("#selectChallenge").val();
    arrRetos = [];
    arrGrados = [];
    arrTotalesRetos = [];
    arrTotalesGrados = [];
    /* Datos del promedio */
    prom_arrRetos = [];
    prom_arrGrados = [];
    prom_arrTotalesRetos = [];
    prom_arrTotalesGrados = [];
    data.forEach(el => {
        if((el.initialsGrade == gradeSelect || gradeSelect == '0') && (el.challengeNumber == challengeSelect || challengeSelect == '0')){
            if(el.totalResourcesDetonatedByGrade){
                if(isNaN(arrRetos[el.challengeNumber])){
                    arrRetos[el.challengeNumber] = parseInt(el.totalResourcesDetonatedByGrade);
                    arrTotalesRetos[el.challengeNumber] = parseInt(el.totalResourcesByGrade);
                    }
                else {
                    arrRetos[el.challengeNumber] += parseInt(el.totalResourcesDetonatedByGrade);
                    arrTotalesRetos[el.challengeNumber] += parseInt(el.totalResourcesByGrade);
                }
                if(isNaN(arrGrados[el.initialsGrade])) {
                    arrGrados[el.initialsGrade] = parseInt(el.totalResourcesDetonatedByGrade);
                    arrTotalesGrados[el.initialsGrade] = parseInt(el.totalResourcesByGrade);
                }
                else {
                    arrGrados[el.initialsGrade] += parseInt(el.totalResourcesDetonatedByGrade);
                    arrTotalesGrados[el.initialsGrade] += parseInt(el.totalResourcesByGrade);
                }
            }
        }
    });
    promData.forEach(el => {
        if((el.initialsGrade == gradeSelect || gradeSelect == '0') && (el.challengeNumber == challengeSelect || challengeSelect == '0')){
            if(el.totalResourcesDetonatedByGrade){
                if(isNaN(prom_arrRetos[el.challengeNumber])){
                    prom_arrRetos[el.challengeNumber] = parseInt(el.totalResourcesDetonatedByGrade);
                    prom_arrTotalesRetos[el.challengeNumber] = parseInt(el.totalResourcesByGrade);
                    }
                else {
                    prom_arrRetos[el.challengeNumber] += parseInt(el.totalResourcesDetonatedByGrade);
                    prom_arrTotalesRetos[el.challengeNumber] += parseInt(el.totalResourcesByGrade);
                }
                if(isNaN(prom_arrGrados[el.initialsGrade])) {
                    prom_arrGrados[el.initialsGrade] = parseInt(el.totalResourcesDetonatedByGrade);
                    prom_arrTotalesGrados[el.initialsGrade] = parseInt(el.totalResourcesByGrade);
                }
                else {
                    prom_arrGrados[el.initialsGrade] += parseInt(el.totalResourcesDetonatedByGrade);
                    prom_arrTotalesGrados[el.initialsGrade] += parseInt(el.totalResourcesByGrade);
                }
            }
        }
    });
    if(myChartGrade) myChartGrade.destroy();
    if(myChartChallenge) myChartChallenge.destroy();
    const challenge = [
        'Reto 1',
        'Reto 2',
        'Reto 3',
        'Reto 4',
        'Reto 5',
        'Reto 6',
        'Reto 7',
        'Reto 8'
    ];
    const dataChallenge = {
        labels: challenge,
        datasets: [
            {
            type: 'line',
            label: 'Promedio',
            backgroundColor: '#7460EE',
            borderColor: '#7460EE',
            data: [Math.round(prom_arrRetos[1]/prom_arrTotalesRetos[1]*100),
                    Math.round(prom_arrRetos[2]/prom_arrTotalesRetos[2]*100),
                    Math.round(prom_arrRetos[3]/prom_arrTotalesRetos[3]*100),
                    Math.round(prom_arrRetos[4]/prom_arrTotalesRetos[4]*100),
                    Math.round(prom_arrRetos[5]/prom_arrTotalesRetos[5]*100),
                    Math.round(prom_arrRetos[6]/prom_arrTotalesRetos[6]*100),
                    Math.round(prom_arrRetos[7]/prom_arrTotalesRetos[7]*100),
                    Math.round(prom_arrRetos[8]/prom_arrTotalesRetos[8]*100)]
        },
        {
            type: 'bar',
            label: '% de recursos por reto',
            backgroundColor: '#26c6da',
            borderColor: '#26c6da',
            data: [Math.round(arrRetos[1]/arrTotalesRetos[1]*100),
                    Math.round(arrRetos[2]/arrTotalesRetos[2]*100),
                    Math.round(arrRetos[3]/arrTotalesRetos[3]*100),
                    Math.round(arrRetos[4]/arrTotalesRetos[4]*100),
                    Math.round(arrRetos[5]/arrTotalesRetos[5]*100),
                    Math.round(arrRetos[6]/arrTotalesRetos[6]*100),
                    Math.round(arrRetos[7]/arrTotalesRetos[7]*100),
                    Math.round(arrRetos[8]/arrTotalesRetos[8]*100)]
        }
    ]
    };
    const configChallenge = {
        // type: 'bar',
        data: dataChallenge,
        options: {
            responsive:true,
            scales: {
                y: {
                    max: 100,
                    min: 0,
                    ticks: {
                        stepSize: 10
                    }
                }
            }
        }
    };
    myChartChallenge = new Chart(
        document.getElementById('resourcesByChallenge'),
        configChallenge
    );
    const grades = ['K1','K2','K3','PF','E1','E2','E3','E4','E5','E6','M7','M8','M9'];
    const dataGrade = {
        labels: grades,
        datasets: [
            {
            type: 'line',
            label: 'Promedio',
            backgroundColor: '#7460EE',
            borderColor: '#7460EE',
            data: [ Math.round(prom_arrGrados['K1']/prom_arrTotalesGrados['K1']*100),
                    Math.round(prom_arrGrados['K2']/prom_arrTotalesGrados['K2']*100),
                    Math.round(prom_arrGrados['K3']/prom_arrTotalesGrados['K3']*100),
                    Math.round(prom_arrGrados['PF']/prom_arrTotalesGrados['PF']*100),
                    Math.round(prom_arrGrados['E1']/prom_arrTotalesGrados['E1']*100),
                    Math.round(prom_arrGrados['E2']/prom_arrTotalesGrados['E2']*100),
                    Math.round(prom_arrGrados['E3']/prom_arrTotalesGrados['E3']*100),
                    Math.round(prom_arrGrados['E4']/prom_arrTotalesGrados['E4']*100),
                    Math.round(prom_arrGrados['E5']/prom_arrTotalesGrados['E5']*100),
                    Math.round(prom_arrGrados['E6']/prom_arrTotalesGrados['E6']*100),
                    Math.round(prom_arrGrados['M7']/prom_arrTotalesGrados['M7']*100),
                    Math.round(prom_arrGrados['M8']/prom_arrTotalesGrados['M8']*100),
                    Math.round(prom_arrGrados['M9']/prom_arrTotalesGrados['M9']*100)
                ]
        },
        {
            type: 'bar',
            label: '% de recursos por grado',
            backgroundColor: '#26c6da',
            borderColor: '#26c6da',
            data: [ Math.round(arrGrados['K1']/arrTotalesGrados['K1']*100),
                    Math.round(arrGrados['K2']/arrTotalesGrados['K2']*100),
                    Math.round(arrGrados['K3']/arrTotalesGrados['K3']*100),
                    Math.round(arrGrados['PF']/arrTotalesGrados['PF']*100),
                    Math.round(arrGrados['E1']/arrTotalesGrados['E1']*100),
                    Math.round(arrGrados['E2']/arrTotalesGrados['E2']*100),
                    Math.round(arrGrados['E3']/arrTotalesGrados['E3']*100),
                    Math.round(arrGrados['E4']/arrTotalesGrados['E4']*100),
                    Math.round(arrGrados['E5']/arrTotalesGrados['E5']*100),
                    Math.round(arrGrados['E6']/arrTotalesGrados['E6']*100),
                    Math.round(arrGrados['M7']/arrTotalesGrados['M7']*100),
                    Math.round(arrGrados['M8']/arrTotalesGrados['M8']*100),
                    Math.round(arrGrados['M9']/arrTotalesGrados['M9']*100),
                ]
        }]
    };
    const configGrade = {
        type: 'bar',
        data: dataGrade,
        options: {
            responsive:true,
            scales: {
                y: {
                    max: 100,
                    min: 0,
                    ticks: {
                        stepSize: 10
                    }
                }
            }
        }
    };
    myChartGrade = new Chart(
        document.getElementById('resourcesByGrade'),
        configGrade
    );
}

let getVersionData = (data) => {
    let iosData = [];
    let iosDataTotal = [];
    let deviceData = [];
    let deviceDataTotal = [];
    let versionData = [];
    let versionDataTotal = [];
    data.map(function(element, index){
        //iOS
        if( iosData.indexOf(element.iosDevice) >= 0) {
            iosDataTotal[ iosData.indexOf(element.iosDevice) ]++;
        } else {
            iosData.push(element.iosDevice);
            iosDataTotal.push(1);
        }
        //Device
        if( deviceData.indexOf(element.device) >= 0) {
            deviceDataTotal[ deviceData.indexOf(element.device) ]++;
        } else {
            deviceData.push(element.device);
            deviceDataTotal.push(1);
        }
        //Version
        if( versionData.indexOf(element.appVersion) >= 0) {
            versionDataTotal[ versionData.indexOf(element.appVersion) ]++;
        } else {
            versionData.push(element.appVersion);
            versionDataTotal.push(1);
        }
    });
    //iOS
    insertGraph('chartiOS','iOS', iosData, iosDataTotal, 'pie', 'iosApp');
    //Device
    insertGraph('chartDevice','Device', deviceData, deviceDataTotal, 'pie', 'DispositivosApp');
    //Version
    insertGraph('chartVersion','Version', versionData, versionDataTotal, 'pie', 'versionApp');
}
const insertGraph = (chartName, titleChart, labelsChart, dataChart, typeChart, idChart) => {
    eval('var myChart'+chartName);
    eval('if(myChart'+chartName+') myChart'+chartName+'.destroy()')
    const labels = labelsChart;
    const allData = {
        labels: labels,
        datasets: [{
            label: titleChart,
            backgroundColor: [
                'rgb(30 136 229)',
                'rgb(116 96 238)',
                'rgb(38 198 218)',
                'rgb(255 177 43)',
                'rgb(30 136 229)',
                'rgb(116 96 238)',
                'rgb(38 198 218)',
                'rgb(255 177 43)',
                'rgb(30 136 229)',
                'rgb(116 96 238)',
                'rgb(38 198 218)',
                'rgb(255 177 43)',
                'rgb(30 136 229)',
                'rgb(116 96 238)',
                'rgb(38 198 218)',
                'rgb(255 177 43)',
            ],
            // borderColor: '#26c6da',
            data: dataChart
        }]
    };
    const myChartConfig = {
        type: typeChart,
        data: allData,
        options: {
            responsive:true,
            scales: {
                y: {
                    // max: 100,
                    min: 0,
                    ticks: {
                        stepSize: 10
                    }
                }
            }
        }
    };
    eval('myChart'+chartName+'= new Chart( document.getElementById(idChart), myChartConfig)'); 
}
$('#selectByGrade .btn').on('click',function(){
    $('#selectByGrade button.active').addClass('btn-outline-primary').removeClass('active');
    $(this).removeClass('btn-outline-primary').addClass('active');
    getAllResource();
});
let getAllResource = () => {
    const challengeResource = $('.nav-link.active').data('reto');
    const gradeResource = $('#selectByGrade button.active').data('grade');
    $('#contentResourcesByChallenge #R'+challengeResource).html('');
    let index2 = 0;
    let challengeSize = 0;
    catResources.map((el, index) =>{
        if(el.Reto == challengeResource && el.grade == gradeResource){
            challengeSize += parseInt(el.fileSize);
            $('#contentResourcesByChallenge #R'+el.Reto).append(`
                <h3 style="position: relative;" data-challenge="${el.idChallenge_int}" data-resource="${el.idResource_int}">
                <span class="col-12 label label-info">${index2+1}: ${el.resource} - unityTarget:${el.unityTarget} - ${(el.fileSize/1000000).toFixed(2)} MB </span>
                <div class="viewed">0</div>
                </h3>`);
            index2++;
        }
    });
    let $elRes;
    catResourcesDetoned.map((el,index)=>{
        $elRes = $(`.tab-pane.active h3[data-challenge="${el.idChallenge_int}"][data-resource="${el.idResource_int}"]`);
        if ( $elRes.length > 0 ) {
            $elRes.children('.viewed').text(el.viewed);
        }else{
            console.log("no vinculado");
            if(el.Reto == challengeResource && el.grade == gradeResource){
                $('#contentResourcesByChallenge #R'+el.Reto).append(`
                    <h3 style="position: relative;" data-challenge="${el.idChallenge_int}" data-resource="${el.idResource_int}">
                    <span class="col-12 label label-danger">${index+1}: ${el.resource} - unityTarget:${el.unityTarget} - ${(el.fileSize/1000000).toFixed(2)} MB</span>
                    <div class="viewed">${el.viewed}</div>
                    </h3>`);
            }
        }
    });
    $('#contentResourcesByChallenge #R'+challengeResource).prepend(`Peso de la descarga del reto: ${(challengeSize/1000000).toFixed(2)} MB`);
}