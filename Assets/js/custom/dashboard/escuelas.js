const getPeriods = () => {
    var objDataPeriod = "";
    var period = "";
    var jqxhr = $.get( base_url+"home/getPeriods", function(data) {
        objDataPeriod = JSON.parse(data);
        if(objDataPeriod.status){
            $("#selectPeriod").html('');
            period = "";
            $.each(objDataPeriod.data, function(key,value) {
                $("#selectPeriod").append(`<option value="${value.idPeriod_int}">${value.periodName}</option>`);
            });
            if(period = localStorage.getItem('period')){
                $(`#selectPeriod option[value="${period}"]`).prop('selected', true);
                getSchools(period);
            }
        }
        else{
            alert(objDataChallenge.msg);
        }
    }).fail(function(){
        $("#selectPeriod").html(`<option value="error">Error al cargar filtros</option>`);
    });
}
getPeriods();
$("#btn_filter").on("click",function(){
    var idPeriod = $("#selectPeriod").val();
    localStorage.setItem('period', idPeriod);
    getSchools(idPeriod);
});
const getSchools = (idPeriod_int) => {
    var jqxhr = $.get( base_url+"dashboard/getSchoolsByPeriod?idPeriod_int="+idPeriod_int, function(data) {
        objDataPeriod = JSON.parse(data);
        if(objDataPeriod.status){
            resourceTable.clear();
            $.each(objDataPeriod.data, function(key,value) {
                resourceTable.row.add([
                    key+1,
                    `<a href="./campus?idCampus_int=${value.idCampus_int}&idPeriod_int=${idPeriod_int}">${value.campusName}</a><br>`,
                    `<span class="mail-desc">${(value.businessCycleCode ? value.businessCycleCode: 'N/A')}</span>`,
                    `<span class="mail-desc">${(value.program ? value.program : 'N/A')}</span>`,
                    Math.round((value.reto1 === null ? 0 : value.reto1)*100)+'%',
                    Math.round((value.reto2 === null ? 0 : value.reto2)*100)+'%',
                    Math.round((value.reto3 === null ? 0 : value.reto3)*100)+'%',
                    Math.round((value.reto4 === null ? 0 : value.reto4)*100)+'%',
                    Math.round((value.reto5 === null ? 0 : value.reto5)*100)+'%',
                    Math.round((value.reto6 === null ? 0 : value.reto6)*100)+'%',
                    Math.round((value.reto7 === null ? 0 : value.reto7)*100)+'%',
                    Math.round((value.reto8 === null ? 0 : value.reto8)*100)+'%',
                    Math.round((value.totDetoned === null ? 0 : value.totDetoned)*100)+'%'
                ]);
            });
            resourceTable.draw();
        }
        else{
            alert(objDataChallenge.msg);
        }
    }).fail(function(){
        alert(objDataPeriod.msg);
    });
}