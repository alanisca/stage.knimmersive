<?php 
	class Mysql extends Conexion{
		private $conexion;
		private $conexionCentral;
		private $conexionSabana;
		private $strquery;
		private $arrValues;
		private $insert;
		private $result;
		private $update;

		function __construct(){
			$this->conexion = new Conexion();
			$this->conexionCentral = $this->conexion->conectCentral();
			$this->conexionSabana = $this->conexion->conectSabana();
			$this->conexion = $this->conexion->conect();
		}
        //Bitacora insertar un registro
        public function bitacora(array $arrValues){
            $this->strQuery = "INSERT INTO bitacora(accion, id_usuario) VALUES(?,?)";
            $this->arrValues = $arrValues;

            $insert = $this->conexion->prepare($this->strQuery);
            
            $resInsert = $insert->execute($this->arrValues);
            if($resInsert){
                $lastInsert = $this->conexion->lastInsertId();
            }
            else{
                $lastInsert = 0;
            }
            return $lastInsert;
        }
		//Insertar un registro
		public function insert(string $DBName, string $query, array $arrValues){
			$this->strquery = $query;
			$this->arrVAlues = $arrValues;
            switch($DBName) {
                case 'stg':
                    $this->insert = $this->conexion->prepare($this->strquery);
                    break;
                case 'central':
                    $this->insert = $this->conexionCentral->prepare($this->strquery);
                    break;
                case 'sabana':
                    $this->insert = $this->conexionSabana->prepare($this->strquery);
                    break;
                default:
                    break;
            }
        	$resInsert = $this->insert->execute($this->arrVAlues);
        	if($resInsert){
	        	$lastInsert = $this->conexion->lastInsertId();
	        }else{
	        	$lastInsert = 0;
	        }
	        return $lastInsert; 
		}
		//Busca un registro
		public function select(string $DBName, string $query){
			$this->strquery = $query;
        	switch($DBName) {
                case 'stg':
                    $this->result = $this->conexion->prepare($this->strquery);
                    break;
                case 'central':
                    $this->result = $this->conexionCentral->prepare($this->strquery);
                    break;
                case 'sabana':
                    $this->result = $this->conexionSabana->prepare($this->strquery);
                    break;
                default:
                    break;
            }
			$this->result->execute();
        	$data = $this->result->fetch(PDO::FETCH_ASSOC);
        	return $data;
		}
		//Devuelve todos los registros
		public function select_all(string $DBName, string $query){
			$this->strquery = $query;
        	switch($DBName) {
                case 'stg':
                    $this->result = $this->conexion->prepare($this->strquery);
                    break;
                case 'central':
                    $this->result = $this->conexionCentral->prepare($this->strquery);
                    break;
                case 'sabana':
                    $this->result = $this->conexionSabana->prepare($this->strquery);
                    break;
                default:
                    break;
            }
			$this->result->execute();
        	$data = $this->result->fetchall(PDO::FETCH_ASSOC);
        	return $data;
		}
		//Actualiza registros
		public function update(string $DBName, string $query, array $arrValues){
			$this->strquery = $query;
			$this->arrVAlues = $arrValues;
			switch($DBName) {
                case 'stg':
                    $this->update = $this->conexion->prepare($this->strquery);
                    break;
                case 'central':
                    $this->update = $this->conexionCentral->prepare($this->strquery);
                    break;
                case 'sabana':
                    $this->update = $this->conexionSabana->prepare($this->strquery);
                    break;
                default:
                    break;
            }
			$resExecute = $this->update->execute($this->arrVAlues);
	        return $resExecute;
		}
		//Eliminar un registros
		public function delete(string $DBName, string $query){
			$this->strquery = $query;
        	switch($DBName) {
                case 'stg':
                    $this->result = $this->conexion->prepare($this->strquery);
                    break;
                case 'central':
                    $this->result = $this->conexionCentral->prepare($this->strquery);
                    break;
                case 'sabana':
                    $this->result = $this->conexionSabana->prepare($this->strquery);
                    break;
                default:
                    break;
            }
			$del = $this->result->execute();
        	return $del;
		}
	}
?>