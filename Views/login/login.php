<!DOCTYPE html>
<html lang="es">
<head>
    <?= headerAdmin($data); ?>
    <link href="<?= media(); ?>css/login.css" id="theme" rel="stylesheet">
    <script src="<?= media(); ?>plugins/aframe.min.js"></script>
</head>
<body>
    <div class="load"></div>
    <div class="boycontent">
        <div class="boy"></div>
    </div>
    <a-scene do-something-once-loaded vr-mode-ui="enabled: false;" id="escena" device-orientation-permission-ui="enabled: false">
            <a-entity id="system" scale="0 0 0" 
                    rotation="-10 0 0"
                    position="0 -5 0"
                    animation__init_2="property:position; to:0 -4 0; dur:1000; delay:2500"
                    animation__init="property:scale; to:.8 .8 .8; dur:1000; delay:2500;">
            <a-sky color="" opacity="0"></a-sky>
            <a-sphere id="sun" position="0 0 0"
                        animation="property: rotation; to:0 360 0; loop: true; dur:50000; easing: linear;"
                        radius="1.4" 
                        src="Assets/planets/0_sun@2x.jpeg">
            </a-sphere>
            <a-sphere position="0 0 0" radius="0" rotation="40 180 0" animation="property: rotation; to:400 540 0; loop: true; dur:4880; easing: linear;">
                <a-sphere   radius="0.1;"
                            position="1.5 0 0"
                            animation="property: rotation; dur:140800; to:0 360 0; loop: true; easing: linear;"
                            animation__init="property: scale; from: 0 0 0; to:1 1 1; easing: linear; dur:1000; delay:2000;"
                            src="Assets/planets/1_mercury@2x.jpeg">
                </a-sphere>
            </a-sphere>
        <a-sphere position="0 0 0" radius="0" rotation="0 45 0" animation="property: rotation; to:0 405 0; loop: true; dur:4250; easing: linear;">
            <a-sphere   radius="0.2"
                        position="2 0 0"
                        animation="property: rotation; dur:583200; to:0 360 0; loop: true; easing: linear;"
                        src="Assets/planets/2_venus@2x.jpeg">
            </a-sphere>
        </a-sphere>
        <a-sphere position="0 0 0" radius="0" rotation="0 0 0" animation="property: rotation; to:0 360 0; loop: true; dur:6650; easing: linear;">
            <a-sphere   radius="0.21"
                        position="2.7 0 0"
                        animation="property: rotation; dur:4000; to:0 360 0; loop: true; easing: linear;"
                        src="Assets/planets/3_earth@2x.jpeg">
                        <a-sphere  radius="0.001" animation="property: rotation; dur:2000; to:0 360 0; loop: true; easing: linear;">
                            <a-sphere position=".3 0 0"
                                    radius="0.07"
                                    animation="property: rotation; dur:58300; to:0 360 0; loop: true; easing: linear;"
                                    src="Assets/planets/3.1_moon@2x.jpeg">
                                </a-sphere>
                        </a-sphere>
            </a-sphere>
        </a-sphere>
        <a-sphere position="0 0 0" radius="0" rotation="0 -45 0" animation="property: rotation; to:0 315 0; loop: true; dur:6870; easing: linear;">
            <a-sphere   radius="0.2"
                        position="3.5 0 0"
                        animation="property: rotation; dur:2500; to:0 360 0; loop: true; easing: linear;"
                        src="Assets/planets/4_mars@2x.jpeg">
            </a-sphere>
        </a-sphere>
        <a-sphere position="0 0 0" radius="0" rotation="0 135 0" animation="property: rotation; to:0 495 0; loop: true; dur:43290; easing: linear;">
            <a-sphere   radius=".9"
                        position="5 0 0"
                        animation="property: rotation; dur:10000; to:0 360 0; loop: true; easing: linear;"
                        src="Assets/planets/5_jupiter@2x.jpeg">
            </a-sphere>
        </a-sphere>
        <a-sphere position="0 0 0" radius="0" rotation="0 270 0" animation="property: rotation; to:0 630 0; loop: true; dur:43290; easing: linear;">
            <a-sphere   radius=".7"
                        position="5.2 0 0"
                        animation="property: rotation; dur:11000; to:0 360 0; loop: true; easing: linear;"
                        src="Assets/planets/6_saturn@2x.jpeg">
                        <a-torus position="0 0 0"
                                arc="360"
                                rotation="95 0 0"
                                color="#a2927f"
                                radius="1"
                                radius-tubular="0.02"
                                radius="1"
                                src=""></a-torus>
            </a-sphere>
        </a-sphere>
        <a-sphere position="0 0 0" radius="0" rotation="0 200 0" animation="property: rotation; to:0 560 0; loop: true; dur:90070; easing: linear;">
            <a-sphere   radius="0.3"
                        position="7 0 0"
                        animation="property: rotation; dur:1700; to:0 360 0; loop: true; easing: linear;"
                        src="Assets/planets/7_uranus@2x.jpeg">
            </a-sphere>
        </a-sphere>
        <a-sphere position="0 0 0" radius="0" rotation="0 0 0" animation="property: rotation; to:0 360 0; loop: true; dur:10986; easing: linear;">
            <a-sphere   radius="0.29"
                        position="8 0 0"
                        animation="property: rotation; dur:1600; to:0 360 0; loop: true; easing: linear;"
                        src="Assets/planets/8_neptune@2x.jpeg">
            </a-sphere>
        </a-sphere>
            
        </a-entity>
        <a-entity id="camera" camera  position="0 0 10" rotation="-2 0 0"></a-entity>
    </a-scene>
    <div class="container">
        <div class="logo"></div>
        <a href='<?= $data["google_client"]->createAuthUrl(); ?>' class='submit circle' type='submit'></a>
    </div>
    <h6 class="copyright">Algunos Derechos Reservados© Knotion® S.A. de C.V.<br>Av. Paseo del Punhuato #1466. Col. Tres Marías C.P. 58254, Morelia, Michoacán, México 2017.</h6>
    <?= footerAdmin($data); ?>
    <!-- ============================================================== -->
    <!-- Style switcher -->
    <!-- ============================================================== -->
    <script src="<?= media(); ?>plugins/sticky-kit-master/dist/sticky-kit.js"></script>
    <script src="<?= media(); ?>js/login.js"></script>
</body>
</html>