<!DOCTYPE html>
<html lang="en">
<head>
    <?= headerAdmin($data); ?>
</head>
<body class="fix-header card-no-border logo-center">
    <?= preloadAdmin(); ?>
    <div id="main-wrapper">
        <?= topBarAdmin(); ?>
        <?= navAdmin($data); ?>
        <div class="page-wrapper">
            <div class="container-fluid">
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center hidden-md-down">
                        <h3 class="text-themecolor m-b-0 m-t-0"><?= $data["page_title"]; ?></h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Inicio</a></li>
                        </ol>
                    </div>
                    <div class="col-md-7 col-12 align-self-center">
                        <div class="d-flex m-r-20 m-l-10 ">
                            <div class="card-body collapse show input-form">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="input-group">
                                            <select class="form-control custom-select col-md-5" name="pais" id="pais" id="selectPeriod">
                                                <option value="1">México</option>
                                            </select>
                                            <span class="input-group-btn">
                                                <button class="btn btn-info form-control custom-select col-md-2" type="button" id="btn_filter">Filtrar</i></button>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row page-titles">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-hover" id="requisiciones">
                                        <thead>
                                            <tr>
                                                <th class=""><h6>Version</h6></th>
                                                <th class="d-none d-sm-none d-md-block "><h6>Descripción</h6></th>
                                                <th class=""><h6>-</h6></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <form action="/catalogoImm/saveVersion" method="post" type="submit" id="formVersion">
                                                    <th><input type="text" id="txtVersion" placeholder="0.0.0" class="form-control form-control-line" name="version" maxlength="8" required></th>
                                                    <th><input type="text" id="txtDescripcion" placeholder="Escribir descripción" class="form-control form-control-line" name="descripcion" maxlength="100" required></th>
                                                    <th><button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Guardar</button></th>
                                                </form>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 
            </div>
            <footer class="footer">© 2022 KnImmersive Knotion Team</footer>
        </div>
    </div>
    <?= footerAdmin($data); ?>
    <script src="<?= media(); ?>plugins/Chart.js/Chart.min.js"></script>
    <script src="<?= media(); ?>js/custom/immersive/version.js"></script>
</body>
</html>