<!DOCTYPE html>
<html lang="en">
<head>
    <?= headerAdmin($data); ?>
    <style>
        table .round{
            font-size: 10px;
        }
    </style>
</head>

<body class="fix-header card-no-border logo-center">
    <?= preloadAdmin(); ?>
    <div id="main-wrapper">
        <?= topBarAdmin(); ?>
        <?= navAdmin($data); ?>
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center hidden-md-down">
                        <h3 class="text-themecolor m-b-0 m-t-0"><?= $data["page_title"]; ?></h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Inicio</a></li>
                        </ol>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-2">
                                        <div class="demo-switch-title">Total</div>
                                        <!-- <div class="switch"><label><input type="checkbox" class="myCheckBox" id="recursos-check" data-id="" checked=""><span class="lever switch-col-purple"></span></label></div> -->
                                        <div class="demo-switch-title" id="recursos-tot">-</div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="demo-switch-title">Targets</div>
                                        <div class="switch"><label><input type="checkbox" class="myCheckBox" id="targets-check" data-id="target" checked=""><span class="lever switch-col-purple"></span></label></div>
                                        <div class="demo-switch-title" id="targets-tot">-</div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="demo-switch-title">AR</div>
                                        <div class="switch"><label><input type="checkbox" class="myCheckBox" id="ar-check" data-id="App. Augmented Reality" checked=""><span class="lever switch-col-purple"></span></label></div>
                                        <div class="demo-switch-title" id="ar-tot">-</div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="demo-switch-title">VR</div>
                                        <div class="switch"><label><input type="checkbox" class="myCheckBox" id="vr-check" data-id="IMM Virtual Reality" checked=""><span class="lever switch-col-purple"></span></label></div>
                                        <div class="demo-switch-title" id="vr-tot">-</div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="demo-switch-title">Interactive</div>
                                        <div class="switch"><label><input type="checkbox" class="myCheckBox" id="interactive-check" data-id="IMM Interactive" checked=""><span class="lever switch-col-purple"></span></label></div>
                                        <div class="demo-switch-title" id="interactive-tot">-</div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="demo-switch-title">Prime</div>
                                        <div class="switch"><label><input type="checkbox" class="myCheckBox" id="prime-check" data-id="IMM Prime" checked=""><span class="lever switch-col-purple"></span></label></div>
                                        <div class="demo-switch-title" id="prime-tot">-</div>
                                    </div>
                                </div>
                                <div class="table-responsive m-t-20">
                                    <table class="table stylish-table" id="table-catalgo">
                                        <thead>
                                            <tr>
                                                <th>Unity target</th>
                                                <th>Recurso</th>
                                                <th>Botón</th>
                                                <th>Tipo de recurso</th>
                                                <th>-</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>    
            </div>
            <footer class="footer">© 2022 KnImmersive Knotion Team</footer>
        </div>
    </div>
    <?= footerAdmin($data); ?>
    <script src="<?= media(); ?>plugins/datatables/jquery.dataTables.min.js" type="text/javascript"></script>
    <script>
        let catalogoTable;
        $( document ).ready(function() {
            catalogoTable = $('#table-catalgo').DataTable({
                "scrollY": "400px",
                "scrollCollapse": true,
                // paging: false,
                // ordering: false,
                // info: false,
                "language": {
                    "lengthMenu": "Mostrar _MENU_ regitros por página",
                    "zeroRecords": "Sin coincidencias - sorry",
                    "info": "Viendo pagina _PAGE_ de _PAGES_",
                    "infoEmpty": "Sin registros disponibles",
                    "sSearch": "Buscar: ",
                    "infoFiltered": "(entre los _MAX_ registros)",
                }
            });
        });
    </script>
    <script src="<?= media(); ?>js/custom/immersive/catalogo.js"></script>
</body>
</html>