<!DOCTYPE html>
<html lang="en">

<head>
<?= headerAdmin($data); ?>
</head>

<body class="fix-header card-no-border logo-center">
    <?= preloadAdmin(); ?>
    <div id="main-wrapper">
        <?= topBarAdmin(); ?>
        <?= navAdmin($data); ?>
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-6 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0"><?= $data["page_title"]; ?></h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Inicio</a></li>
                        </ol>
                    </div>
                    <div class="col-6 align-self-center">
                        <div class="d-flex justify-content-end">
                            <div class="d-flex m-r-20 m-l-10">
                                <div class="card-body collapse show input-form">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="input-group">
                                                <select class="form-control custom-select col-md-5" id="selectPeriod">
                                                    <option value="">Filtrar periodo</option>
                                                </select>
                                                <!-- <select class="form-control custom-select col-md-5" id="selectCampus">
                                                    <option value="">Filtrar campus</option>
                                                </select> -->
                                                <span class="input-group-btn">
                                                    <button class="btn btn-info form-control custom-select col-md-2" type="button" id="btn_filter">Filtrar</i></button>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body" >
                                <div class="table-responsive">
                                    <table id="campusTable" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Campus</th>
                                                <th>Campaña</th>
                                                <th>Program</th>
                                                <th>1</th>
                                                <th>2</th>
                                                <th>3</th>
                                                <th>4</th>
                                                <th>5</th>
                                                <th>6</th>
                                                <th>7</th>
                                                <th>8</th>
                                                <th>Total</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>    
            </div>
            <footer class="footer">© 2022 KnImmersive Knotion Team</footer>
        </div>
    </div>
    <?= footerAdmin($data); ?>
    <script src=""></script>
    <script src="<?= media(); ?>plugins/Chart.js/Chart.min.js"></script>
    <script src="<?= media(); ?>plugins/datatables/jquery.dataTables.min.js" type="text/javascript"></script>
    <script>
        var resourceTable;
        $( document ).ready(function() {
            resourceTable = $('#campusTable').DataTable({
                // "scrollY": "400px",
                "scrollCollapse": true,
                // paging: false,
                // ordering: false,
                // info: false,
                "language": {
                    "lengthMenu": "Mostrar _MENU_ regitros por página",
                    "zeroRecords": "Sin coincidencias - sorry",
                    "info": "Viendo pagina _PAGE_ de _PAGES_",
                    "infoEmpty": "Sin registros disponibles",
                    "sSearch": "Buscar: ",
                    "infoFiltered": "(entre los _MAX_ registros)",
                }
            });
            $('.dataTables_length').addClass('bs-select');
        });
    </script>
    <script src="<?= media(); ?>js/custom/dashboard/escuelas.js"></script>
    
    <script></script>
</body>
</html>