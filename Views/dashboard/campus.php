<!DOCTYPE html>
<html lang="en">

<head>
<?= headerAdmin($data); ?>
<style>
    .viewed {
        position: absolute;
        background-color: red;
        color: #fff;
        width: 20px;
        height: 20px;
        border-radius: 50%;
        display: flex;
        justify-content: center;
        align-items: center;
        font-size: 11px;
        right: -10px;
        top: -5px;
    }
</style>
</head>
<body class="fix-header card-no-border logo-center">
    <?= preloadAdmin(); ?>
    <div id="main-wrapper">
        <?= topBarAdmin(); ?>
        <?= navAdmin($data); ?>
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!--           Bread crumb and right sidebar toggle                 -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-6 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0" id="campusName" idCampus_int="<?= $data['idCampus']; ?>" idPeriod_int="<?= $data['idPeriod']; ?>"></h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item" id="schoolName">Campus</li>
                        </ol>
                    </div>
                    <div class="col-6 align-self-center">
                        <div class="d-flex justify-content-end">
                            <div class="d-flex m-r-20 m-l-10 col-12">
                                <div class="card-body collapse show input-form">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="input-group">
                                                <select class="form-control custom-select col-6" id="selectChallenge">
                                                    <option value="0">Todos los retos</option>
                                                    <option value="1">Reto 1</option>
                                                    <option value="2">Reto 2</option>
                                                    <option value="3">Reto 3</option>
                                                    <option value="4">Reto 4</option>
                                                    <option value="5">Reto 5</option>
                                                    <option value="6">Reto 6</option>
                                                    <option value="7">Reto 7</option>
                                                    <option value="8">Reto 8</option>
                                                </select>
                                                <select class="form-control custom-select col-6" id="selectGrade">
                                                    <option value="0">Todos los grados</option>
                                                    <option value="K1">K1</option>
                                                    <option value="K2">K2</option>
                                                    <option value="K3">K3</option>
                                                    <option value="PF">PF</option>
                                                    <option value="E1">E1</option>
                                                    <option value="E2">E2</option>
                                                    <option value="E3">E3</option>
                                                    <option value="E4">E4</option>
                                                    <option value="E5">E5</option>
                                                    <option value="E6">E6</option>
                                                    <option value="M7">M7</option>
                                                    <option value="M8">M8</option>
                                                    <option value="M9">M9</option>
                                                </select>
                                                <span class="input-group-btn">
                                                    <button class="btn btn-info form-control custom-select col-md-2" type="button" id="btn_filter">Filtrar</i></button>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>  
                <div class="row">
                    <div class="col-lg-6 col-md-6">
                        <div class="card">
                            <div class="m-t-0"></div>
                            <canvas id="resourcesByChallenge" width="603" height="301" style="display: block; box-sizing: border-box; height: 301px; width: 603px;"></canvas>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <div class="card">
                            <div class="m-t-0"></div>
                            <canvas id="resourcesByGrade" width="603" height="301" style="display: block; box-sizing: border-box; height: 301px; width: 603px;"></canvas>
                        </div>
                    </div>
                    <!-- Immersive APP Version -->
                    <div class="col-lg-4 col-md-4 col-12">
                        <div class="card">
                            <div class="card-body">
                                <h3 class="card-title">Versión</h3>
                                <h6 class="card-subtitle">Versión de la app de Immersive</h6>
                                <canvas id="versionApp" width="351" height="351" style="display: block; box-sizing: border-box; height: 351px; width: 351px;"></canvas>
                            </div>
                            <div>
                                <hr class="m-t-0 m-b-0">
                            </div>
                        </div>
                    </div>
                    <!-- Devices -->
                    <div class="col-lg-4 col-md-4 col-12">
                        <div class="card">
                            <div class="card-body">
                                <h3 class="card-title">Dispositivos</h3>
                                <h6 class="card-subtitle">Dispositivos utilizados</h6>
                                <canvas id="DispositivosApp" width="351" height="351" style="display: block; box-sizing: border-box; height: 351px; width: 351px;"></canvas>
                            </div>
                            <div>
                                <hr class="m-t-0 m-b-0">
                            </div>
                        </div>
                    </div>
                    <!-- iOS -->
                    <div class="col-lg-4 col-md-4 col-12">
                        <div class="card">
                            <div class="card-body">
                                <h3 class="card-title">iOS</h3>
                                <h6 class="card-subtitle">Versión de iOS utilizada en los Dispositivos</h6>
                                <canvas id="iosApp" width="351" height="351" style="display: block; box-sizing: border-box; height: 351px; width: 351px;"></canvas>
                            </div>
                            <div>
                                <hr class="m-t-0 m-b-0">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">UnityBundles vinculados por Reto y Grado</h4>
                                <h6 class="card-subtitle">Los registros son actualizados manualmente de la BD Centra de Knotion semanalmente.</code></h6>
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs" role="tablist">
                                    <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#R1" role="tab" aria-expanded="true" data-reto="1"><span class="hidden-sm-up"><i class=""></i></span> <span class="hidden-xs-down">Reto 1</span></a></li>
                                    <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#R2" role="tab" aria-expanded="false" data-reto="2"><span class="hidden-sm-up"><i class=""></i></span> <span class="hidden-xs-down">Reto 2</span></a> </li>
                                    <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#R3" role="tab" aria-expanded="false" data-reto="3"><span class="hidden-sm-up"><i class=""></i></span> <span class="hidden-xs-down">Reto 3</span></a> </li>
                                    <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#R4" role="tab" aria-expanded="false" data-reto="4"><span class="hidden-sm-up"><i class=""></i></span> <span class="hidden-xs-down">Reto 4</span></a> </li>
                                    <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#R5" role="tab" aria-expanded="false" data-reto="5"><span class="hidden-sm-up"><i class=""></i></span> <span class="hidden-xs-down">Reto 5</span></a> </li>
                                    <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#R6" role="tab" aria-expanded="false" data-reto="6"><span class="hidden-sm-up"><i class=""></i></span> <span class="hidden-xs-down">Reto 6</span></a> </li>
                                    <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#R7" role="tab" aria-expanded="false" data-reto="7"><span class="hidden-sm-up"><i class=""></i></span> <span class="hidden-xs-down">Reto 7</span></a> </li>
                                    <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#R8" role="tab" aria-expanded="false" data-reto="8"><span class="hidden-sm-up"><i class=""></i></span> <span class="hidden-xs-down">Reto 8</span></a> </li>
                                </ul>
                                <div class="button-group mt-2" id="selectByGrade" style="text-align: center;">
                                    <button type="button" class="btn waves-effect waves-light btn-rounded btn-primary btn-outline-primary" data-grade="K1">K1</button>
                                    <button type="button" class="btn waves-effect waves-light btn-rounded btn-primary btn-outline-primary" data-grade="K2">K2</button>
                                    <button type="button" class="btn waves-effect waves-light btn-rounded btn-primary btn-outline-primary" data-grade="K3">K3</button>
                                    <button type="button" class="btn waves-effect waves-light btn-rounded btn-primary btn-outline-primary" data-grade="PF">PF</button>
                                    <button type="button" class="btn waves-effect waves-light btn-rounded btn-primary btn-outline-primary" data-grade="E1">E1</button>
                                    <button type="button" class="btn waves-effect waves-light btn-rounded btn-primary btn-outline-primary" data-grade="E2">E2</button>
                                    <button type="button" class="btn waves-effect waves-light btn-rounded btn-primary btn-outline-primary" data-grade="E3">E3</button>
                                    <button type="button" class="btn waves-effect waves-light btn-rounded btn-primary btn-outline-primary" data-grade="E4">E4</button>
                                    <button type="button" class="btn waves-effect waves-light btn-rounded btn-primary btn-outline-primary" data-grade="E5">E5</button>
                                    <button type="button" class="btn waves-effect waves-light btn-rounded btn-primary btn-outline-primary" data-grade="E6">E6</button>
                                    <button type="button" class="btn waves-effect waves-light btn-rounded btn-primary btn-outline-primary" data-grade="M7">M7</button>
                                    <button type="button" class="btn waves-effect waves-light btn-rounded btn-primary btn-outline-primary" data-grade="M8">M8</button>
                                    <button type="button" class="btn waves-effect waves-light btn-rounded btn-primary btn-outline-primary" data-grade="M9">M9</button>
                                </div>
                                <!-- Tab panes -->
                                <div class="tab-content" id="contentResourcesByChallenge">
                                    <div class="tab-pane p-20 active" id="R1" role="tabpanel" aria-expanded="true"></div>
                                    <div class="tab-pane p-20" id="R2" role="tabpanel" aria-expanded="false"></div>
                                    <div class="tab-pane p-20" id="R3" role="tabpanel" aria-expanded="false"></div>
                                    <div class="tab-pane p-20" id="R4" role="tabpanel" aria-expanded="false"></div>
                                    <div class="tab-pane p-20" id="R6" role="tabpanel" aria-expanded="false"></div>
                                    <div class="tab-pane p-20" id="R7" role="tabpanel" aria-expanded="false"></div>
                                    <div class="tab-pane p-20" id="R8" role="tabpanel" aria-expanded="false"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>          
            </div>
            <footer class="footer">© 2022 KnImmersive Knotion Team</footer>
        </div>
    </div>
    <?= footerAdmin($data); ?>
    <script src=""></script>
    <script src="<?= media(); ?>plugins/Chart.js/Chart.min.js"></script>
    <script src="<?= media(); ?>js/custom/dashboard/campus.js"></script>
    <script>

    </script>
</body>
</html>