<!DOCTYPE html>
<html lang="en">
<head>
    <?= headerAdmin($data); ?>
</head>

<body class="fix-header card-no-border logo-center">
    <?= preloadAdmin(); ?>
    <div id="main-wrapper">
        <?= topBarAdmin(); ?>
        <?= navAdmin($data); ?>
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center hidden-md-down">
                        <h3 class="text-themecolor m-b-0 m-t-0"><?= $data["page_title"]; ?></h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Inicio</a></li>
                        </ol>
                    </div>
                    <div class="col-md-7 col-12 align-self-center">
                        <div class="d-flex justify-content-end">
                            <div class="d-flex m-r-20 m-l-10 ">
                                <div class="card-body collapse show input-form">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="input-group">
                                                <select class="form-control custom-select col-md-5" id="selectPeriod">
                                                    <option value="">Filtrar periodo</option>
                                                </select>
                                                <!-- <select class="form-control custom-select col-md-5" id="selectCampus">
                                                    <option value="">Filtrar campus</option>
                                                </select> -->
                                                <span class="input-group-btn">
                                                    <button class="btn btn-info form-control custom-select col-md-2" type="button" id="btn_filter">Filtrar</i></button>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <!-- Column -->
                    <div class="col-lg-3 col-sm-6 col-xs-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex flex-row">
                                    <div class="round round-lg align-self-center round-info"><i class="mdi mdi-cloud-check"></i></div>
                                    <div class="m-l-10 align-self-center">
                                        <h3 class="m-b-0 font-light" id="version">-</h3>
                                        <h5 class="text-muted m-b-0">Versión IMM</h5></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="col-lg-3 col-sm-6 col-xs-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex flex-row">
                                    <div class="round round-lg align-self-center round-warning"><i class="mdi mdi-cube-outline"></i></div>
                                    <div class="m-l-10 align-self-center">
                                        <h3 class="m-b-0 font-lgiht" id="tot_recurosos">-</h3>
                                        <h5 class="text-muted m-b-0">Recursos</h5></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="col-lg-3 col-sm-6 col-xs-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex flex-row">
                                    <div class="round round-lg align-self-center round-primary"><i class="mdi mdi-domain"></i></div>
                                    <div class="m-l-10 align-self-center">
                                        <h3 class="m-b-0 font-lgiht" id="active_campus">-</h3>
                                        <h5 class="text-muted m-b-0" id="tot_campus">Campus</h5></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="col-lg-3 col-sm-6 col-xs-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex flex-row">
                                    <div class="round round-lg align-self-center round-danger"><i class="mdi mdi-account"></i></div>
                                    <div class="m-l-10 align-self-center">
                                        <h3 class="m-b-0 font-lgiht" id="tot_alumnos">-</h3>
                                        <h5 class="text-muted m-b-0">Alumnos</h5></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body" >
                                <!-- <div > -->
                                    <canvas id="dateResourceChart"></canvas>
                                <!-- </div> -->
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="card">
                            <div class="card-body">
                                <div>
                                    <canvas id="challengeResourcesChart"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="card">
                            <div class="card-body">
                                <div>
                                    <canvas id="gradeResourcesChart"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>    
            </div>
            <footer class="footer">© 2022 KnImmersive Knotion Team</footer>
        </div>
    </div>
    <?= footerAdmin($data); ?>
    <script src=""></script>
    <script src="<?= media(); ?>plugins/Chart.js/Chart.min.js"></script>
    <script src="<?= media(); ?>js/custom/home/home.js"></script>
    <script>
        // $( document ).ready(function() {
            // const challenge = [
            //     'Reto 1',
            //     'Reto 2',
            //     'Reto 3',
            //     'Reto 4',
            //     'Reto 5',
            //     'Reto 6',
            //     'Reto 7',
            //     'Reto 8'
            // ];
            // const dataChallenge = {
            //     labels: challenge,
            //     datasets: [{
            //         label: 'Recursos por reto',
            //         backgroundColor: '#26c6da',
            //         borderColor: '#26c6da',
            //         data: [97, 79, 40, 10, 5, 2, 0, 0],
            //     }]
            // };
            // const configChallenge = {
            //     type: 'bar',
            //     data: dataChallenge,
            //     options: {
            //         responsive:true,
            //         scales: {
            //             y: {
            //                 max: 100,
            //                 min: 0,
            //                 ticks: {
            //                     stepSize: 10
            //                 }
            //             }
            //         }
            //     }
            // };
            // const myChartChallenge = new Chart(
            //     document.getElementById('challengeResourcesChart'),
            //     configChallenge
            // );
        //     const grades = [
        //         'K1',
        //         'K2',
        //         'K3',
        //         'E1',
        //         'E2',
        //         'E3',
        //         'E4',
        //         'E5',
        //         'E6',
        //         'M7',
        //         'M8',
        //         'M9'
        //     ];
        //     const dataGrades = {
        //         labels: grades,
        //         datasets: [{
        //         label: 'Recursos por grado',
        //         backgroundColor: '#26c6da',
        //         borderColor: '#26c6da',
        //         data: [80, 79, 40, 45, 10, 55, 38, 40, 32, 84, 12, 39],
        //         }]
        //     };
        //     const configGrades = {
        //         type: 'bar',
        //         data: dataGrades,
        //         options: {
        //             responsive:true,
        //             scales: {
        //                 y: {
        //                     max: 100,
        //                     min: 0,
        //                     ticks: {
        //                         stepSize: 10
        //                     }
        //                 }
        //             }
        //         }
        //     };
        //     const myChartGrades = new Chart(
        //         document.getElementById('gradeResourcesChart'),
        //         configGrades
        //     );
        // });
    </script>
</body>
</html>