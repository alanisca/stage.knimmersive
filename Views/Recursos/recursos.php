<!DOCTYPE html>
<html lang="en">

<head>
    <?= headerAdmin($data); ?>
</head>

<body class="fix-header card-no-border logo-center">
    <?= preloadAdmin(); ?>
    <div id="main-wrapper">
        <?= topBarAdmin(); ?>
        <?= navAdmin($data); ?>
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center hidden-md-down">
                        <h3 class="text-themecolor m-b-0 m-t-0"><?= $data["page_title"]; ?></h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Inicio</a></li>
                        </ol>
                    </div>
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <form action="" id="buscarForm">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group has-success">
                                                <label class="control-label">Periodo</label>
                                                <select class="form-control custom-select" id="period">
                                                    <option value="16">MX 2022 - 2023</option>
                                                    <option value="26">MX 2023 - 2024</option>
                                                </select>
                                                <small class="form-control-feedback"> Selecciona un periodo </small> 
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group has-success">
                                                <label class="control-label">Reto</label>
                                                <select class="form-control custom-select" id="challenge">
                                                    <option value="1">Reto 1</option>
                                                    <option value="2">Reto 2</option>
                                                    <option value="3">Reto 3</option>
                                                    <option value="4">Reto 4</option>
                                                    <option value="5">Reto 5</option>
                                                    <option value="6">Reto 6</option>
                                                    <option value="7">Reto 7</option>
                                                    <option value="8">Reto 8</option>
                                                </select>
                                                <small class="form-control-feedback"> Selecciona un reto </small> 
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group has-success">
                                                <label class="control-label">Grado</label>
                                                <select class="form-control custom-select" id="grade">
                                                    <option value="-">Todos los grados</option>
                                                    <option value="K1">1º de Kinder</option>
                                                    <option value="K2">2º de Kinder</option>
                                                    <option value="K3">3º de Kinder</option>
                                                    <option value="E1">1º de Primaria</option>
                                                    <option value="E2">2º de Primaria</option>
                                                    <option value="E3">3º de Primaria</option>
                                                    <option value="E4">4º de Primaria</option>
                                                    <option value="E5">5º de Primaria</option>
                                                    <option value="E6">6º de Primaria</option>
                                                    <option value="M7">1º de Secundaria</option>
                                                    <option value="M8">2º de Secundaria</option>
                                                    <option value="M9">3º de Secundaria</option>
                                                </select>
                                                <small class="form-control-feedback"> Selecciona un grado </small> 
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group has-success">
                                                <label class="control-label"></label>
                                                <button type="submit" class="btn btn-success waves-effect waves-light m-t-30">BUSCAR</button><br/>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <table class="resourcesTable" id="resourcesTable" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <!-- <th>#</th> -->
                                                    <th>Recurso</th>
                                                    <th>Grado</th>
                                                    <th>Pathway</th>
                                                    <th>Sesion</th>
                                                    <th>Actividad</th>
                                                    <th>Paso</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="footer">© 2022 KnImmersive Knotion Team</footer>
        </div>
    </div>
    <?= footerAdmin($data); ?>
    <script src="<?= media(); ?>plugins/datatables/jquery.dataTables.min.js" type="text/javascript"></script>
    <script>
        let resourceTable;
        $( document ).ready(function() {
            resourceTable = $('#resourcesTable').DataTable({
                responsive: true,
                // "scrollY": "400px",
                "scrollCollapse": true,
                paging: true,
                ordering: true,
                info: false,
                "language": {
                    "lengthMenu": "Mostrar _MENU_ regitros por página",
                    "zeroRecords": "Sin coincidencias - sorry",
                    "info": "Viendo pagina _PAGE_ de _PAGES_",
                    "infoEmpty": "Sin registros disponibles",
                    "infoFiltered": "(entre los _MAX_ registros)"
                }
            });
        });
    </script>
    <script src="<?= media(); ?>js/custom/recursos/recursos.js"></script>
</body>
</html>