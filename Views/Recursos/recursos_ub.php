<!DOCTYPE html>
<html lang="en">

<head>
<?= headerAdmin($data); ?>
</head>

<body class="fix-header card-no-border logo-center">
    <?= preloadAdmin(); ?>
    <div id="main-wrapper">
        <?= topBarAdmin(); ?>
        <?= navAdmin($data); ?>
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center hidden-md-down">
                        <h3 class="text-themecolor m-b-0 m-t-0"><?= $data["page_title"]; ?></h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Inicio</a></li>
                        </ol>
                    </div>
                    <div class="col-md-7 col-12 align-self-right">
                        <div class="d-flex justify-content-end">
                            <div class="card-body collapse show input-form">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="input-group">
                                            <select class="form-control custom-select col-md-5" id="selectPeriod"></select>
                                            <select class="form-control custom-select col-md-5" id="selectChallenge"></select>
                                            <span class="input-group-btn">
                                                <button class="btn btn-info form-control custom-select col-md-2" type="button" id="filterResource">Filtrar</i></button>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                    <div class="card">
                            <div class="card-body">
                                <center class="m-t-30"> 
                                    <h4 class="card-title m-t-10" id="titleResource">Recursos por reto</h4>
                                    <h6 class="card-subtitle" id="subtitleResource">Filtrar para mostrar datos</h6>
                                </center>
                                <div class="row text-center m-t-20 grades">
                                    <div class="col-3 m-t-20 K1">
                                        <h3 class="m-b-0 font-light">-</h3>
                                        <small>K1</small>
                                    </div>
                                    <div class="col-3 m-t-20 K2">
                                        <h3 class="m-b-0 font-light">-</h3>
                                        <small>K2</small>
                                    </div>
                                    <div class="col-3 m-t-20 K3">
                                        <h3 class="m-b-0 font-light">-</h3>
                                        <small>K3</small>
                                    </div>
                                    <div class="col-3 m-t-20 PF">
                                        <h3 class="m-b-0 font-light">-</h3>
                                        <small>PF</small>
                                    </div>
                                    <div class="col-4 m-t-20 E1">
                                        <h3 class="m-b-0 font-light">-</h3>
                                        <small>E1</small>
                                    </div>
                                    <div class="col-4 m-t-20 E2">
                                        <h3 class="m-b-0 font-light">-</h3>
                                        <small>E2</small>
                                    </div>
                                    <div class="col-4 m-t-20 E3">
                                        <h3 class="m-b-0 font-light">-</h3>
                                        <small>E3</small>
                                    </div>
                                    <div class="col-4 m-t-20 E4">
                                        <h3 class="m-b-0 font-light">-</h3>
                                        <small>E4</small>
                                    </div>
                                    <div class="col-4 m-t-20 E5">
                                        <h3 class="m-b-0 font-light">-</h3>
                                        <small>E5</small>
                                    </div>
                                    <div class="col-4 m-t-20 E6">
                                        <h3 class="m-b-0 font-light">-</h3>
                                        <small>E6</small>
                                    </div>
                                    <div class="col-4 m-t-20 M7">
                                        <h3 class="m-b-0 font-light">-</h3>
                                        <small>M7</small>
                                    </div>
                                    <div class="col-4 m-t-20 M8">
                                        <h3 class="m-b-0 font-light">-</h3>
                                        <small>M8</small>
                                    </div>
                                    <div class="col-4 m-t-20 M9">
                                        <h3 class="m-b-0 font-light">-</h3>
                                        <small>M9</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table id="resourcesTable" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Grado</th>
                                                <th>Recurso</th>
                                                <th>ver</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="footer">© 2022 KnImmersive Knotion Team</footer>
        </div>
    </div>
    <?= footerAdmin($data); ?>
    <script src=""></script>
    <script src="<?= media(); ?>js/custom/recursos/recursos_ub.js"></script>
    <!-- <script src="<?= media(); ?>plugins/bootstrap-table/dist/bootstrap-table.min.js"></script> -->
    <!-- <script src="<?= media(); ?>plugins/bootstrap-table/dist/bootstrap-table.ints.js"></script> -->
    <!-- <script src="<?= media(); ?>plugins/Chart.js/Chart.min.js"></script> -->
    <script src="<?= media(); ?>plugins/datatables/jquery.dataTables.min.js" type="text/javascript"></script>
    <script>
        var resourceTable;
        $( document ).ready(function() {
            resourceTable = $('#resourcesTable').DataTable({
                // "scrollY": "400px",
                "scrollCollapse": true,
                // paging: false,
                // ordering: false,
                // info: false,
                "language": {
                    "lengthMenu": "Mostrar _MENU_ regitros por página",
                    "zeroRecords": "Sin coincidencias - sorry",
                    "info": "Viendo pagina _PAGE_ de _PAGES_",
                    "infoEmpty": "Sin registros disponibles",
                    "infoFiltered": "(entre los _MAX_ registros)"
                }
            });
            $('.dataTables_length').addClass('bs-select');
        });
    </script>
</body>
</html>