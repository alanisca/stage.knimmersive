<!DOCTYPE html>
<html lang="en">

<head>
<?= headerAdmin($data); ?>
</head>

<body class="fix-header card-no-border logo-center">
    <?= preloadAdmin(); ?>
    <div id="main-wrapper">
        <?= topBarAdmin(); ?>
        <?= navAdmin($data); ?>
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0"><?= $data["page_title"]; ?></h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Principal</a></li>
                            <li class="breadcrumb-item active">Secundaria</li>
                        </ol>
                    </div>
                    <div class="col-md-7 col-4 align-self-center">
                        <div class="d-flex m-t-10 justify-content-end">
                            <div class="d-flex m-r-20 m-l-10 hidden-md-down">
                                <div class="chart-text m-r-10">
                                    <h6 class="m-b-0"><small>TOTAL USERS</small></h6>
                                    <h4 class="m-t-0 text-info">58,356</h4></div>
                                <div class="spark-chart">
                                    <div id="monthchart"></div>
                                </div>
                            </div>
                            <div class="d-flex m-r-20 m-l-10 hidden-md-down">
                                <div class="chart-text m-r-10">
                                    <h6 class="m-b-0"><small>USERS CONECTED</small></h6>
                                    <h4 class="m-t-0 text-primary">48,356</h4></div>
                                <div class="spark-chart">
                                    <div id="lastmonthchart"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Tabla: Registros Immersive</h4>
                                <h6 class="card-subtitle">Registros de BD <code>V1.0</code></h6>
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>id_registro</th>
                                                <th>id_kn_usuario</th>
                                                <th>id_recurso</th>
                                                <th>accion</th>
                                                <th>fecha_registro</th>
                                                <th>fecha_sys</th>
                                                <th>version_app</th>
                                                <th>dispositivo</th>
                                                <th>ios</th>
                                                <th>idPeriod_int</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Table: immersiveRegister</h4>
                                <h6 class="card-subtitle">Registros de BD <code>V2.0</code></h6>
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>idimmersiveRegister_int</th>
                                                <th>idUser_int</th>
                                                <th>idChallenge_int</th>
                                                <th>idResource_int</th>
                                                <th>appVersion</th>
                                                <th>device</th>
                                                <th>iosDevice</th>
                                                <th>createTime</th>
                                                <th>updateTime</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="footer">
                © 2022 Dashboard Knotion Immersive App by KnImmersive Knotion Team
            </footer>
        </div>
    </div>
    <?= footerAdmin($data); ?>
</body>
</html>
