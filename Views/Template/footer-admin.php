    <script>
        var clickHandlerDown = ('ontouchstart' in document.documentElement ? "touchstart" : "mousedown");
        var clickHandlerUp = ('ontouchend' in document.documentElement ? "touchend" : "mouseup");
        var clickHandler = ('ontouchstart' in document.documentElement ? "click" : "click");
    </script>

    <script src="<?= media(); ?>plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?= media(); ?>plugins/bootstrap/js/popper.min.js"></script>
    <script src="<?= media(); ?>plugins/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?= media(); ?>js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="<?= media(); ?>js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="<?= media(); ?>js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="<?= media(); ?>plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <script src="<?= media(); ?>plugins/sparkline/jquery.sparkline.min.js"></script>
    <!--Custom JavaScript -->
    <script src="<?= media(); ?>js/custom.min.js"></script>
    <!-- ============================================================== -->
    <!-- Style switcher -->
    <!-- ============================================================== -->
    <script src="<?= media(); ?>plugins/styleswitcher/jQuery.style.switcher.js"></script>
    
    <script>
        const base_url = "<?= base_url();?>";
    </script>
    <script src="<?= media(); ?>js/custom/global.js"></script>