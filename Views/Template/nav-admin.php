        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li class="<?= ($data["page_id"] == 1 ? "active" : ""); ?>">
                            <a class="has-arrow" href="<?= base_url(); ?>" aria-expanded="false"><i class="mdi mdi-home"></i><span class="hide-menu">Inicio </span></a>
                        </li>
                        <li class="<?= ($data["page_id"] == 2 ? "active" : ""); ?>">
                            <a class="has-arrow" href="#" aria-expanded="false"><i class="mdi mdi-gauge"></i><span class="hide-menu">Dashboard </span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="<?= base_url(); ?>dashboard/escuelas">Campus</a></li>
                                <!-- <li><a href="index.html">Usuarios</a></li> -->
                                <!-- <li><a href="index2.html">Recursos</a></li> -->
                            </ul>
                        </li>
                        <li class="<?= ($data["page_id"] == 3 ? "active" : ""); ?>">
                            <a class="has-arrow " href="#" aria-expanded="false"><i class="mdi mdi-bullseye"></i><span class="hide-menu">Immersive</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="<?= base_url(); ?>recursos">Vinculación de recursos</a></li>
                                <li><a href="<?= base_url(); ?>immersive/catalogo">Catálogo de recursos</a></li>
                                <li><a href="<?= base_url(); ?>immersive/version">Versión de la aplicación</a></li>
                            </ul>
                        </li>
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>