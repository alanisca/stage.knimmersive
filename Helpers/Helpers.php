<?php 

	//Retorla la url del proyecto
	function base_url(){
        return BASE_URL;
    }
    function media(){
        return base_url()."Assets/";
    }
    
	//Muestra información formateada
	function dep($data)
    {
        $format  = print_r('<pre>');
        $format .= print_r($data);
        $format .= print_r('</pre>');
        return $format;
    }
    //Elimina exceso de espacios entre palabras
    function strClean($strCadena){
        $string = preg_replace(['/\s+/','/^\s|\s$/'],[' ',''], $strCadena);
        $string = trim($string); //Elimina espacios en blanco al inicio y al final
        $string = stripslashes($string); // Elimina las \ invertidas
        $string = str_ireplace("<script>","",$string);
        $string = str_ireplace("</script>","",$string);
        $string = str_ireplace("<script src>","",$string);
        $string = str_ireplace("<script type=>","",$string);
        $string = str_ireplace("SELECT * FROM","",$string);
        $string = str_ireplace("DELETE FROM","",$string);
        $string = str_ireplace("INSERT INTO","",$string);
        $string = str_ireplace("SELECT COUNT(*) FROM","",$string);
        $string = str_ireplace("DROP TABLE","",$string);
        $string = str_ireplace("OR '1'='1","",$string);
        $string = str_ireplace('OR "1"="1"',"",$string);
        $string = str_ireplace('OR ´1´=´1´',"",$string);
        $string = str_ireplace("is NULL; --","",$string);
        $string = str_ireplace("is NULL; --","",$string);
        $string = str_ireplace("LIKE '","",$string);
        $string = str_ireplace('LIKE "',"",$string);
        $string = str_ireplace("LIKE ´","",$string);
        $string = str_ireplace("OR 'a'='a","",$string);
        $string = str_ireplace('OR "a"="a',"",$string);
        $string = str_ireplace("OR ´a´=´a","",$string);
        $string = str_ireplace("OR ´a´=´a","",$string);
        $string = str_ireplace("--","",$string);
        $string = str_ireplace("^","",$string);
        $string = str_ireplace("[","",$string);
        $string = str_ireplace("]","",$string);
        $string = str_ireplace("==","",$string);
        return $string;
    }
    //Genera una contraseña de 10 caracteres
	function passGenerator($length = 10)
    {
        $pass = "";
        $longitudPass=$length;
        $cadena = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
        $longitudCadena=strlen($cadena);

        for($i=1; $i<=$longitudPass; $i++)
        {
            $pos = rand(0,$longitudCadena-1);
            $pass .= substr($cadena,$pos,1);
        }
        return $pass;
    }
    //Genera un token
    function token()
    {
        $r1 = bin2hex(random_bytes(10));
        $r2 = bin2hex(random_bytes(10));
        $r3 = bin2hex(random_bytes(10));
        $r4 = bin2hex(random_bytes(10));
        $token = $r1.'-'.$r2.'-'.$r3.'-'.$r4;
        return $token;
    }
    //Formato para valores monetarios
    function formatMoney($cantidad){
        $cantidad = number_format($cantidad,2,SPD,SPM);
        return $cantidad;
    }
    
    /* Template */
    function headerAdmin($data=""){
        $view_header = "Views/Template/header-admin.php";
        require_once($view_header);
    }
    function footerAdmin($data=""){
        $view_footer = "Views/Template/footer-admin.php";
        require_once($view_footer);
    }
    function preloadAdmin(){
        $view_preload = "Views/Template/preload-admin.php";
        require_once($view_preload);
    }
    function topBarAdmin(){
        $view_topBar = "Views/Template/topBar-admin.php";
        require_once($view_topBar);
    }
    function navAdmin($data=""){
        $view_nav = "Views/Template/nav-admin.php";
        require_once($view_nav);
    }
    /* Template */
    function convert_from_latin1_to_utf8_recursively($dat){
        if (is_string($dat)) {
            return utf8_encode($dat);
        } elseif (is_array($dat)) {
            $ret = [];
            foreach ($dat as $i => $d) $ret[ $i ] = convert_from_latin1_to_utf8_recursively($d);

            return $ret;
        } elseif (is_object($dat)) {
            foreach ($dat as $i => $d) $dat->$i = convert_from_latin1_to_utf8_recursively($d);

            return $dat;
        } else {
            return $dat;
        }
   }
   function initGoogle(){
    /* GOOGLE SESION */
        //Include Google Client Library for PHP autoload file
        include_once("assets/vendor/autoload.php");
        //Make object of Google API Client for call Google API
        $google_client = new Google_Client();
        //Set the OAuth 2.0 Client ID
        $google_client->setClientId('795745808440-nv9opjp5ob6jmet9dqalmui92nfgf6dg.apps.googleusercontent.com');
        //Set the OAuth 2.0 Client Secret key
        $google_client->setClientSecret('ahm7Duq12CYqpkfyXbDDllfE');
        //Set the OAuth 2.0 Redirect URI
        $google_client->setRedirectUri(BASE_URL.'login/correctLogin');
        //Data
        $google_client->addScope('email');
        $google_client->addScope('profile');
        //start session on web page
        // session_start();
        /* GOOGLE SESION */
        return $google_client;
    }
 ?>