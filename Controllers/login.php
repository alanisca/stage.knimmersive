<?php 
class login extends Controllers{

    public function __construct(){
        parent::__construct();
        session_start();
    }
    public function login(){
        $data['page_title']="IMMERSIVE - Iniciar sesión";
        $data['page_tag']="Iniciar sesión";
        $data['page_title']="¡Pagina no encontrada!";
        $data['page_text']="Es necesario ingresar con tu correo KNOTION";
        $data["google_client"] = $this->model->goSesion();
        $this->views->getView($this,"login",$data);
    }
    public function correctLogin(){
        $login = $this->model->checkLogin($_GET["code"]);
        if($login)
            header("Location:".base_url());
        else{
            header("Location:".base_url()."login");
        }
    }
    public function logout(){
        if($this->model->logoutSession())
            header("Location:".base_url()."login");
    }
}
?>