<?php
	class home extends Controllers{
		public function __construct(){
			parent::__construct();
            // session_start();
            // if(!isset($_SESSION['access_token']))
            //     header("Location:".base_url()."login");
		}

		public function home(){
			$data['page_id'] = 1;
			$data['page_tag'] = "Knotion Immersive";
			$data['page_title'] = "Dashboard Immersive";
			$data['page_description'] = "Pagina principal con los datos generales de la aplicación como: versión, usos, escuelas, usuarios, etc.";
			$this->views->getView($this,"home",$data);
		}
        public function getPeriods(){
            $arrData = $this->model->getPeriods();
            echo json_encode($arrData, JSON_UNESCAPED_UNICODE);
            die();
        }
        public function get_MainData(){
            $idPeriod_int = $_GET['idPeriod_int'];
            $mainArray = array();
            /* Version */
            $arrData = $this->model->imm_version_model();
            if($arrData["status"]) array_push($mainArray, $arrData["data"]);
            
            /* Total de recursos */
            $arrData = $this->model->getResource($idPeriod_int);
            if($arrData["status"]) array_push($mainArray, $arrData["data"]);
            
            /* Campus activos vs total de campus */
            $arrData = $this->model->get_campus($idPeriod_int);
            if($arrData["status"]) array_push($mainArray, $arrData["data"]);

            /* Usuarios activos */
            $arrData = $this->model->get_users($idPeriod_int);
            if($arrData["status"]) array_push($mainArray, $arrData["data"]);
            echo json_encode($mainArray, JSON_UNESCAPED_UNICODE);

            die();
        }
        public function get_graphicDataChallenge(){
            $idPeriod_int = $_GET['idPeriod_int'];
            $graphicArray = array();
            $arrData = $this->model->get_resourcesByChallenge($idPeriod_int);
            echo json_encode($arrData, JSON_UNESCAPED_UNICODE);
            die();
        }
        public function get_graphicDataGrade(){
            $idPeriod_int = $_GET['idPeriod_int'];
            $graphicArray = array();
            $arrData = $this->model->get_resourcesByGrade($idPeriod_int);
            echo json_encode($arrData, JSON_UNESCAPED_UNICODE);
            die();
        }
        public function getData_Date(){
            $arrData = $this->model->getData_DateModel($_GET['idPeriod_int']);
            echo json_encode($arrData, JSON_UNESCAPED_UNICODE);
            die();
        }
	}
?>