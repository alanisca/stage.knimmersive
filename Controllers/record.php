<?php
class record extends Controllers{
    public $userName;
    public $unityTarget;
    public $accion;
    public $date;
    public $version_app;
    public $dispositivo;
    public $ios;
    public $school;
    public $idUserInt;
    public $idPeriodInt;
    public $Period;
    public $Campus;
    public $Challenge;

    public function __construct(){
        parent::__construct();
    }
    public function record(){
        /* VERIFICAMOS LA VARIABLES QUE SE RECIBEN POR GET */
        $arrRegistro = array(
            'userName' => ( (isset($_GET["userName"]) && !empty($_GET["userName"]) ) ? $_GET["userName"] : null ),
            'unityTarget' => ( (isset($_GET["unityTarget"]) && !empty($_GET["unityTarget"]) ) ? $_GET["unityTarget"] : null ),
            'accion' => ( (isset($_GET["accion"]) && !empty($_GET["accion"]) ) ? $_GET["accion"] : null ),
            'date' => ( (isset($_GET["date"]) && !empty($_GET["date"]) ) ? $_GET["date"] : null ),
            'version_app' => ( (isset($_GET["version_app"]) && !empty($_GET["version_app"]) ) ? $_GET["version_app"] : null ),
            'dispositivo' => ( (isset($_GET["dispositivo"]) && !empty($_GET["dispositivo"]) ) ? $_GET["dispositivo"] : null ),
            'ios' => ( (isset($_GET["ios"]) && !empty($_GET["ios"]) ) ? $_GET["ios"] : null ),
            'school' => ( (isset($_GET["school"]) && !empty($_GET["school"]) ) ? $_GET["school"] : null ),
            'idUserInt' => ( (isset($_GET["idUserInt"]) && !empty($_GET["idUserInt"]) ) ? $_GET["idUserInt"] : null ),
            'idPeriodInt' => ( (isset($_GET["idPeriodInt"]) && !empty($_GET["idPeriodInt"]) ) ? $_GET["idPeriodInt"] : null ),
            'Period' => ( (isset($_GET["Period"]) && !empty($_GET["Period"]) ) ? $_GET["Period"] : null ),
            'Campus' => ( (isset($_GET["Campus"]) && !empty($_GET["Campus"]) ) ? $_GET["Campus"] : null ),
            'Challenge' => ( (isset($_GET["Challenge"]) && !empty($_GET["Challenge"]) ) ? $_GET["Challenge"] : null )
        );
        $arrData = $this->model->newRecord($arrRegistro);
        echo json_encode($arrData, JSON_UNESCAPED_UNICODE);
        die();
    }
    public function imm_version(){
        if( isset($_GET['id_pais']) && !empty($_GET['id_pais']) ){
            $id_pais_version = $_GET['id_pais'];
        }
        else{
            $id_pais_version = 1;
        }
        $arrData = $this->model->imm_version_model($id_pais_version);
        echo json_encode($arrData, JSON_UNESCAPED_UNICODE);
        die();
    }
}
?>

