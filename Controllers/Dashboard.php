<?php
	class dashboard extends Controllers{
		public function __construct(){
			parent::__construct();
            session_start();
            if(!isset($_SESSION['access_token']))
                header("Location:".base_url()."login");
		}
		public function dashboard(){
			$data['page_id'] = 2;
			$data['page_tag'] = "Knotion Immersive";
			$data['page_title'] = "Dashboard Immersive";
			$data['page_description'] = "Pagina principal con los datos generales de la aplicación como: versión, usos, escuelas, usuarios, etc.";
			$this->views->getView($this,"home",$data);
		}
        public function escuelas(){
            $data['page_id'] = 2;
			$data['page_tag'] = "Knotion Immersive";
			$data['page_title'] = "Dashboard escuelas";
			$data['page_description'] = "Cat谩logo de escuelas/campus por periodo";
			$this->views->getView($this,"escuelas",$data);
        }
        public function getSchoolsByPeriod(){
            $idPeriod = ((isset($_GET["idPeriod_int"]) && !empty(($_GET["idPeriod_int"])) ) ? $_GET["idPeriod_int"] : null);
            $arrData = $this->model->getSchoolsByPeriod($idPeriod);
            echo json_encode($arrData, JSON_UNESCAPED_UNICODE);
            die();
        }
        public function campus(){
            $data['page_id'] = 2;
			$data['page_tag'] = "Knotion Immersive";
			$data['page_title'] = "Campus";
			$data['page_description'] = "Datos generales de campus ";

            $data["idCampus"] = ((isset($_GET["idCampus_int"]) && !empty(($_GET["idCampus_int"])) ) ? $_GET["idCampus_int"] : null);
            $data["idPeriod"] = ((isset($_GET["idPeriod_int"]) && !empty(($_GET["idPeriod_int"])) ) ? $_GET["idPeriod_int"] : null);
            $this->views->getView($this,"campus", $data);
        }
        public function campusInfo(){
            $idCampus = ((isset($_GET["idCampus_int"]) && !empty(($_GET["idCampus_int"])) ) ? $_GET["idCampus_int"] : null);
            $idPeriod = ((isset($_GET["idPeriod_int"]) && !empty(($_GET["idPeriod_int"])) ) ? $_GET["idPeriod_int"] : null);
            $mainData = array();
            /* General Data */
            $arrData = $this->model->getGeneralDataCampus($idCampus,$idPeriod);
            array_push($mainData, $arrData);
            /* Retos y grados */
            $arrData = $this->model->getCampusInfo($idCampus,$idPeriod);
            array_push($mainData, $arrData);
            /* Version APP, Dispositivo & iOS */
            $arrData = $this->model->getAppVersionData($idCampus,$idPeriod);
            array_push($mainData, $arrData);
            /* Total de recursos disponibles por grado y reto */
            $arrData = $this->model->getResource($idCampus,$idPeriod);
            array_push($mainData, $arrData);
            /* Total de recursos detonados */
            $arrData = $this->model->getResourceDetoned($idCampus,$idPeriod);
            if($arrData["status"]) array_push($mainData, $arrData);
            else array_push($mainData, []);
            /* Promedio de Retos y grados */
            $arrData = $this->model->getPromedio($idPeriod);
            array_push($mainData, $arrData);

            echo json_encode($mainData, JSON_UNESCAPED_UNICODE);
            die();
        }
        public function getResourceDetoned(){
            $idCampus = ((isset($_GET["idCampus_int"]) && !empty(($_GET["idCampus_int"])) ) ? $_GET["idCampus_int"] : null);
            $idPeriod = ((isset($_GET["idPeriod_int"]) && !empty(($_GET["idPeriod_int"])) ) ? $_GET["idPeriod_int"] : null);
            $arrData = $this->model->getResourceDetoned($idCampus,$idPeriod);
            echo json_encode($arrData, JSON_UNESCAPED_UNICODE);
            die();
        }

        /* Version */
        public function saveVersion(){
            if($_POST){
                if(empty($_POST['version']) || empty($_POST['descripcion'])){
                    $arrResponse = array("status" => false, "msg" => "Datos incorrectos o vacios");
                }
                else{
                    $version = strClean($_POST['version']);
                    $descripcion = strClean($_POST['descripcion']);
                    $id_pais_version = $_GET['id_pais'];
                    $arrData = $this->model->saveVersion($version, $descripcion, $id_pais_version);
                    $arrResponse = array("status" => $arrData["status"], "msg" => $arrData["msg"]);
                }
            }
            else{
                $arrResponse = array("status" => false, "msg" => "Datos incorrectos");
            }
            echo json_encode($arrResponse, JSON_UNESCAPED_UNICODE);
            die();
        }
        public function getVersions(){
            $id_pais_version = $_GET['id_pais'];
            $arrData = $this->model->getVersions($id_pais_version);
            $arrResponse = array('status' => true, 'data' => $arrData,'msg' => 'Datos encontrados');
            echo json_encode($arrResponse, JSON_UNESCAPED_UNICODE);
            die();
        }
        public function changeVersions(){
            if(empty($_GET["v"])){
                $arrResponse = array("status" => false, "msg" => "Datos incorrectos o vacios");
            }
            else{
                $version = strClean($_GET["v"]);
                $id_pais_version = $_GET['id_pais'];
                $arrData = $this->model->changeVersion($version, $id_pais_version);
                $arrResponse = array("status" => $arrData["status"], "msg" => $arrData["msg"]);
            }
            echo json_encode($arrResponse, JSON_UNESCAPED_UNICODE);
            die();
        }
	}
?>