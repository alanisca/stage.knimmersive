<?php
class immersive extends Controllers{
    public function __construct(){
        parent::__construct();
        session_start();
            if(!isset($_SESSION['access_token']))
                header("Location:".base_url()."login");
    }
    /* version */
    public function version(){
        $data['page_title']="IMMERSIVE - version";
        $data['page_page']="Version pendientes";
        $data['page_title']="¡Version!";
        $data['page_text']="Version de producción";
		$data['page_id'] = 3;
        $this->views->getView($this,"version",$data);
    }
    public function getVersions(){
        $idCountry = ((isset($_GET["idPais_int"]) && !empty(($_GET["idPais_int"])) ) ? $_GET["idPais_int"] : null);
        $arrData = $this->model->getVersions($idCountry);
        echo json_encode($arrData, JSON_UNESCAPED_UNICODE);
        die();
    }
    /* Catálogo de recursos */
    public function catalogo(){
        $data['page_title']="IMMERSIVE - Catálogo de recursos";
        $data['page_page']="Recursos Immersive";
        $data['page_title']="Catálogo de recursos!";
        $data['page_text']="Catálogo de recursos";
		$data['page_id'] = 3;
        $this->views->getView($this,"catalogo",$data);
    }
    public function getCatalogo(){
        $arrData = $this->model->getCatalogo();
        echo json_encode($arrData, JSON_UNESCAPED_UNICODE);
        die();
    }
}
?>