<?php 
	class exportar extends Controllers{
        public $inicioRegistros;
        public $totalRegistros;
		public function __construct(){
			parent::__construct();
            ini_set('memory_limit', '-1');
		}
		public function exportar(){
			$data['page_id'] = 2;
			$data['page_tag'] = "Exportar registros";//Texto en pestaña de navegador
			$data['page_title'] = "Exportar ";
			$data['page_title'] = "Exportar registros a Central";
			$this->views->getView($this,"Exportar",$data);
		}
        public function upDateImmersiveRegister(){
            while ($this->model->getUpdateImmersiveRegister()["status"]) {
                $arrDataGet = $this->model->getUpdateImmersiveRegister();
                if($arrDataGet["status"]){
                    $arrData = $this->model->upDateImmersiveRegister($arrDataGet["data"]["idResource_int"], $arrDataGet["data"]["idChallenge_int"]);
                    echo json_encode($arrData, JSON_UNESCAPED_UNICODE);
                    // die();
                }
                else{
                    echo json_encode("Sin registros para cambios");
                    die();
                }
            }
            echo json_encode("Se termina");
            die();
        }
        
        public function getRegistro(){
            $arrData = $this->model->selectRegistros();
            foreach($arrData as $key => $value){
                foreach($value as $key2 => $value2){
                    echo $value2."<br>";
                    $this->getChallenge($value2);
                }
            }
            // echo json_encode($arrData, JSON_UNESCAPED_UNICODE);
            // die();
        }
        public function getChallenge(int $idRegistro){
            // if((isset($_GET["idRegistro"]) && !empty($_GET["idRegistro"])) )){
            if((isset($idRegistro) && !empty($idRegistro))){
                // $idRegistro = $_GET["idRegistro"];
                $arrData = $this->model->exportRegistro($idRegistro);
                // echo json_encode($arrData, JSON_UNESCAPED_UNICODE);
                // die();
            }
            else{
                echo "Registro no valido";
            }
        }
	}
 ?>