<?php
	class recursos extends Controllers{
		public function __construct(){
			parent::__construct();
            // session_start();
            // if(!isset($_SESSION['access_token']))
            //     header("Location:".base_url()."login");
		}
		public function recursos(){
			$data['page_id'] = 3;
            $data['subpage_id'] = 3;
			$data['page_tag'] = "Recursos vinculados Immersive";
			$data['page_title'] = "Recursos vinculados de Immersive";
			$data['page_description'] = "Recursos vinculados de immersive por ciclo, grado";
			$this->views->getView($this,"recursos",$data);
		}
        public function getResource(){
            $idPeriod_int = ( (isset($_GET["idPeriod_int"]) && !empty($_GET["idPeriod_int"]) ) ? $_GET["idPeriod_int"] : "-" );
            $challengeNumber = ( (isset($_GET["challengenumber"]) && !empty($_GET["challengenumber"]) ) ? $_GET["challengenumber"] : 0 );
            $gradeInitials = ( (isset($_GET["gradeinitials"]) && !empty($_GET["gradeinitials"]) ) ? $_GET["gradeinitials"] : 0 );
            $arrData = $this->model->getResource($idPeriod_int, $challengeNumber, $gradeInitials);
            echo json_encode($arrData, JSON_UNESCAPED_UNICODE);
            die();
        }
	}
?>