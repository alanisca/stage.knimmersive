<?php

class getfileimm extends Controllers{
    public function __construct(){
        parent::__construct();
    }
    public function getfile($fileName){
        $arrData = $this->model->getFile($fileName);
        if($arrData["status"]){
            header("Location: ".$arrData["url"]);
            die();
        }else{
            echo trim($arrData["url"]);
        }
    }
    public function getfileimm($fileName){
        echo $fileName;
    }
}
?>