<?php
class dashboardModel extends Mysql{
    public function __construct(){
        parent::__construct();
    }
    public function getSchoolsByPeriod(int $idPeriod){
        $sql = "SELECT idPeriod_int, 
                    periodName, 
                    idCampus_int, 
                    campusName, 
                    businessCycleCode, 
                    program, 
                    SUM(if(challengeNumber = 1, totalResourcesDetonatedByGrade, 0)) / SUM(if(challengeNumber = 1, totalResourcesByGrade, 0)) as reto1, 
                    SUM(if(challengeNumber = 2, totalResourcesDetonatedByGrade, 0)) / SUM(if(challengeNumber = 2, totalResourcesByGrade, 0)) as reto2, 
                    SUM(if(challengeNumber = 3, totalResourcesDetonatedByGrade, 0)) / SUM(if(challengeNumber = 3, totalResourcesByGrade, 0)) as reto3, 
                    SUM(if(challengeNumber = 4, totalResourcesDetonatedByGrade, 0)) / SUM(if(challengeNumber = 4, totalResourcesByGrade, 0)) as reto4, 
                    SUM(if(challengeNumber = 5, totalResourcesDetonatedByGrade, 0)) / SUM(if(challengeNumber = 5, totalResourcesByGrade, 0)) as reto5, 
                    SUM(if(challengeNumber = 6, totalResourcesDetonatedByGrade, 0)) / SUM(if(challengeNumber = 6, totalResourcesByGrade, 0)) as reto6, 
                    SUM(if(challengeNumber = 7, totalResourcesDetonatedByGrade, 0)) / SUM(if(challengeNumber = 7, totalResourcesByGrade, 0)) as reto7, 
                    SUM(if(challengeNumber = 8, totalResourcesDetonatedByGrade, 0)) / SUM(if(challengeNumber = 8, totalResourcesByGrade, 0)) as reto8, 
                    SUM(totalResourcesDetonatedByGrade)/SUM(totalResourcesByGrade) as totDetoned, 
                    SUM(totalResourcesByGrade) as totalResources
                FROM sabana 
                WHERE idPeriod_int = $idPeriod
                GROUP BY idPeriod_int, periodName, idCampus_int, campusName, businessCycleCode, program
                ORDER BY 15 desc";
        if($request = $this->select_all('sabana', $sql)){
            return array("status"=>true,"data"=>$request);
        }
        return array("status"=>false,"msg"=>"No se encontraron datos en este periodo");
    }
    public function getGeneralDataCampus(int $idCampus_int, int $idPeriod_int){
        $sql = "SELECT Campus.idCampus_int, Campus.name as campus
                FROM Campus 
                WHERE Campus.idCampus_int = $idCampus_int";
        if($request = $this->select_all('sabana',$sql)){
            return array("status"=>true, "data"=>$request);
        }
        else{
            return array("status"=>false, "msg"=>"No se encontraron datos de este campus");
        }
    }
    public function getCampusInfo(int $idCampus_int, int $idPeriod_int){
        $sql = "SELECT challengeNumber, 
                        initialsGrade, 
                        totalResourcesByGrade, 
                        totalResourcesDetonatedByGrade
                    FROM sabana
                WHERE idCampus_int = $idCampus_int AND idPeriod_int = $idPeriod_int
                order by challengeNumber, initialsGrade";
        if($request = $this->select_all('sabana', $sql)){
            return array("status"=>true, "data"=>$request);
        }
        else{
            return array("status"=>false, "msg"=>"No se encontraron datos");
        }
    }
    public function getPromedio(int $idPeriod_int){
        $sql = "SELECT challengeNumber, 
                    initialsGrade, 
                    TRUNCATE(AVG(totalResourcesByGrade),0) as totalResourcesByGrade,
                    TRUNCATE(AVG(totalResourcesDetonatedByGrade),0) as totalResourcesDetonatedByGrade
                FROM sabana
                WHERE sabana.idPeriod_int = $idPeriod_int AND totalResourcesDetonatedByGrade > 0 AND idCampus_int != 157
                GROUP BY challengeNumber, initialsGrade
                ORDER BY challengeNumber, initialsGrade;";
        if($request = $this->select_all('sabana', $sql)){
            return array("status"=>true, "data"=>$request);
        }
        else{
            return array("status"=>false, "msg"=>"No se encontraron datos");
        }
    }
    public function getAppVersionData(int $idCampus_int, int $idPeriod_int){
        $sql = "SELECT MAX(ImmersiveRegister.idImmersiveRegister_int) as id, 
                ImmersiveRegister.idUser_int, 
                User.userName, 
                ImmersiveRegister.appVersion, 
                ImmersiveRegister.device, 
                ImmersiveRegister.iosDevice
        FROM ImmersiveRegister 
        INNER JOIN User ON ImmersiveRegister.idUser_int = User.idUser_int AND User.idCampus_int = $idCampus_int AND ImmersiveRegister.idPeriod_int = $idPeriod_int
        GROUP BY idUser_int";
        if($request = $this->select_all('sabana',$sql)){
            return array("status"=>true, "data"=>$request);
        }
        else{
            return array("status"=>false, "msg"=>"No se encontraron datos");
        }
    }
    public function getResource(int $idCampus_int, int $idPeriod_int){
        //Selecciona los unityBundles vinculados a ese periodo
        $sql = "select 	ad.idCampus_int, 
                        ch.idGrade_int, 
                        gr.initials as grade, 
                        ch.number as Reto, 
                        ch.idChallenge_int as idChallenge_int, 
                        re.idResource_int as idResource_int,
                        re.title as resource,
                        re.unityTarget as unityTarget,
                        re.fileSize as fileSize
                from Challenge ch 
                    inner join Challenge_UnityBundle cub on ( cub.idChallenge_int=ch.idChallenge_int and ch.idPeriod_int = $idPeriod_int ) 
                    inner join Resource re on ( re.idResource_int=cub.idResource_int and re.idResourceType_int in (49, 58, 230, 231, 229) ) 
                    inner join AgreementDetail ad on (ad.idPeriodPayment = $idPeriod_int and ad.idGrade_int=ch.idGrade_int) 
                    inner join Grade gr on ( ch.idGrade_int = gr.idGrade_int ) 
                where ad.idCampus_int = $idCampus_int 
                AND re.idResource_int NOT IN ('15782','111932','111933','111934','111935','111936','111937','111938','111939','111940','111941','111942','111944','111945','111947','111949','111950','111958','111959','111960','111961','111962','111963','111964','111965','111966','111967','111968','111969','111972','111973','111974','111975','115843','127740','131010','172187','172188','172189','172190','172191','172192','172193','172194','172195','172196','172197','172198','172199','172200','172201','172202','172203','172204','172205','172206','172207','172208','172209','172210','172211','172212','172213','172214','172215','172216','172217','172218','178719','214532')
                AND re.title not like '%.mp4%'
                ORDER BY ch.number, gr.initials, re.title;";
        if($request = $this->select_all('central',$sql)){
            return array("status"=>true, "data"=>$request);
        }
        else{
            return array("status"=>false, "msg"=>"No se encontraron datos");
        }
    }

    public function getResourceDetoned(int $idCampus_int, int $idPeriod_int){
        $sql = "SELECT User.idUser_int 
            FROM User
            INNER JOIN Campus_User ON User.idUser_int = Campus_User.idUser_int
            WHERE Campus_User.idCampus_int = $idCampus_int";
        if($request = $this->select_all('central',$sql)){
            $idUsers = "";
            foreach($request as $user => $valor){
                foreach ($valor as $key => $value) {
                    $idUsers = $idUsers.$value.',';
                }
            }
            $idUsers = substr($idUsers, 0, -1);
            $sql2 = "SELECT Period.idPeriod_int as idPeriod_int,
                            ImmersiveRegister.idChallenge_int as idChallenge_int,
                            ImmersiveRegister.idResource_int,
                            Challenge.number as Reto,
                            Grade.initials as grade,
                            resource.title as resource,
                            resource.unityTarget as unityTarget,
                            resource.fileSize as fileSize,
                            count(distinct idUser_int) as viewed
                    FROM ImmersiveRegister
                        INNER JOIN Challenge ON ImmersiveRegister.idChallenge_int = Challenge.idChallenge_int
                        INNER JOIN Grade ON Challenge.idGrade_int = Grade.idGrade_int
                        INNER JOIN resource ON ImmersiveRegister.idResource_int = resource.idResource_int
                        INNER JOIN Period ON Challenge.idPeriod_int = Period.idPeriod_int
                    WHERE ImmersiveRegister.idUser_int IN ($idUsers)
                        AND Period.idPeriod_int = $idPeriod_int 
                        AND ImmersiveRegister.idResource_int NOT IN ('15782','111932','111933','111934','111935','111936','111937','111938','111939','111940','111941','111942','111944','111945','111947','111949','111950','111958','111959','111960','111961','111962','111963','111964','111965','111966','111967','111968','111969','111972','111973','111974','111975','115843','127740','131010','172187','172188','172189','172190','172191','172192','172193','172194','172195','172196','172197','172198','172199','172200','172201','172202','172203','172204','172205','172206','172207','172208','172209','172210','172211','172212','172213','172214','172215','172216','172217','172218','178719','214532')
                    GROUP BY 1,2,3
                    ORDER BY ImmersiveRegister.createTime desc";
            
            if($request2 = $this->select_all('sabana',$sql2)){
                return array("status"=>true, "data"=>$request2);
            }
            else{
                return array("status"=>false, "msg"=>"No se encontraron registros:".$sql2);
            }
        }
        else{
            return array("status"=>false, "msg"=>"No se encontraron datos alumnos:".$sql);
        }
    }
    /* Verison */
    public function saveVersion(string $version, string $descripcion, int $id_pais = 1){
        $sql = "SELECT * FROM kn_version WHERE version = '".$version."' and id_pais = ".$id_pais;
        if($request = $this->select_all('stg', $sql)){
            return array("status"=>false, "msg"=>" Versión ya existe");
        }
        else{
            $queryUpdate =  "UPDATE kn_version SET status = ? WHERE id_pais = ".$id_pais;
            $arrUpdate = Array(0);
            $request = $this->update('stg', $queryUpdate, $arrUpdate);

            $query_insert =  "INSERT INTO kn_version(version,descripcion,status,id_pais) VALUES (?,?,?,?)";
            $arrInsert = Array($version,$descripcion,1,$id_pais);
            $request_insert = $this->insert('stg', $query_insert, $arrInsert);
            $this->bitacora(array("IMM: Nueva version:".$version,$_SESSION['id_user']));
            return array("status" => true, "msg"=>"Versión agregada");
        }
    }
    public function getVersions(int $id_pais = 1){
        $sql = "SELECT * FROM kn_version WHERE id_pais = $id_pais ORDER BY version desc";
        $request = $this->select_all('stg', $sql);
        return $request;
    }
    public function changeVersion(int $version, int $id_pais = 1){
        $queryUpdate =  "UPDATE kn_version SET status = ? WHERE id_pais = ".$id_pais;
        $arrUpdate = Array(0);
        $request = $this->update('stg', $queryUpdate, $arrUpdate);

        $queryUpdate =  "UPDATE kn_version SET status = ? WHERE id_version = ".$version." and id_pais = ".$id_pais;
        $arrUpdate = Array(1);
        $request = $this->update('stg', $queryUpdate, $arrUpdate);

        $this->bitacora(array("IMM: Cambio de version:".$version.", ID:".$version ,$_SESSION['id_user']));
        return array("status" => true, "msg"=>"Cambio de versión");
    }
}
?>