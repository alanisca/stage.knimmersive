<?php

class loginModel extends Mysql{
    public function __construct(){
        parent::__construct();
    }
    public function goSesion(){
        return initGoogle();
    }
    public function logoutSession(){
        $google_client = initGoogle();
        if(isset($_SESSION['id_user']))
            $this->bitacora(array("USUARIO: CERRAR SESION",$_SESSION['id_user']));
        //Reset OAuth access token
        $google_client->revokeToken();
        //Limpiamos variables de sesión
        session_unset();
        //Destroy entire session data.
        session_destroy();
        return true;
    }
    public function getID($correo){
        $sql = "SELECT * FROM usuarios WHERE correo = $id";
        $request = $this->select('stg', $sql);
        return $request;
    }
    public function checkLogin($data){
        $google_client = initGoogle();
        // define('DURACION_SESION','1036800'); //24 horas
        // ini_set("session.cookie_lifetime",DURACION_SESION);
        // ini_set("session.gc_maxlifetime",DURACION_SESION);
        // session_cache_expire(DURACION_SESION);
        session_start();
        session_regenerate_id(true); 
        // session_start();

        if(isset($_GET["code"])){
            //It will Attempt to exchange a code for an valid authentication token.
            $token = $google_client->fetchAccessTokenWithAuthCode($_GET["code"]);
           
            //This condition will check there is any error occur during geting authentication token. If there is no any error occur then it will execute if block of code/
            if(!isset($token['error'])){
                //Set the access token used for requests
                $google_client->setAccessToken($token['access_token']);
                //Store "access_token" value in $_SESSION variable for future use.
                $_SESSION['access_token'] = $token['access_token'];
                //Create Object of Google Service OAuth 2 class
                $google_service = new Google_Service_Oauth2($google_client);
                //Get user profile data from google
                $data = $google_service->userinfo->get();
                //Below you can find Get profile data and store into $_SESSION variable
                if(!empty($data['given_name'])){
                    $_SESSION['user_first_name'] = $data['given_name'];
                }

                if(!empty($data['family_name'])){
                    $_SESSION['user_last_name'] = $data['family_name'];
                }

                if(!empty($data['email'])){
                    $_SESSION['user_email_address'] = $data['email'];
                }

                if(!empty($data['gender'])){
                    $_SESSION['user_gender'] = $data['gender'];
                }
                if(!empty($data['picture'])){
                    $_SESSION['user_image'] = $data['picture'];
                }
            }
        }
        if(!isset($_SESSION['access_token'])){
            return "false";
        }
        else{
            $sql = "SELECT * FROM usuarios WHERE correo = '".$_SESSION['user_email_address']."'";
            $request = $this->select('stg', $sql);
            $_SESSION['id_user'] = $request["id_usuario"];
            $_SESSION['acceso_IMM'] = $request["rol_Immersive"];
            $_SESSION['acceso_Int'] = $request["rol_Interactives"];
            $_SESSION['tipo_usuario'] = $request["rol"];
            if($request){
                if($request["imagen"] != $_SESSION['user_image'] || $request['usuario'] != $_SESSION['user_first_name']."  ".$_SESSION['user_last_name']){
                    $sql = "UPDATE usuarios SET imagen = ?, usuario = ? WHERE id_usuario = '".$_SESSION['id_user']."'";
                    $arrData = array($_SESSION['user_image'], $_SESSION['user_first_name']."  ".$_SESSION['user_last_name']);
                    $request = $this->update('stg', $sql, $arrData);
                    $this->bitacora(array("USUARIO: ACTUALIZADO",$_SESSION['id_user']));
                }
                $this->bitacora(array("USUARIO: INICIO DE SESION",$_SESSION['id_user']));
                return $request;
            }
            else{
                $query_insert = "INSERT INTO usuarios(correo, usuario, imagen, rol_immersive, rol) VALUES(?,?,?)";
                $arrData = array($_SESSION['user_email_address'], $_SESSION['user_first_name']."  ".$_SESSION['user_last_name'], $_SESSION['user_image'], 1, 'invitado');
                $request_insert = $this->insert('stg', $query_insert, $arrData);
                $_SESSION['id_user'] = $request_insert;
                // $this->bitacora(array("USUARIO: NUEVO",$_SESSION['id_user']));
                // $this->bitacora(array("USUARIO: INICIO DE SESION",$_SESSION['id_user']));
                return $request_insert;
            }
        }
    }
}
?>