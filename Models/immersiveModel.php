<?php 
class immersiveModel extends Mysql {
    public function __construct(){
        parent::__construct();
    }
    public function getVersions(int $id_pais = 1){
        $sql = "SELECT * FROM kn_version WHERE id_pais = $id_pais ORDER BY version desc";
        if($request = $this->select_all('stg',$sql)){
            return array('status'=>true, 'data'=>$request);
        }
        else{
            return array('status'=>false,'msg'=>'Problemas al cargar versiones');
        }
    }
    public function getCatalogo(){
        $sql = "SELECT reFile.idResource_int as file_idResource_int,
                        reFile.title as file_title, 
                        reFile.unityTarget as file_unityTarget, 
                        reFile.idResourceType_int as file_idResourceType_int, 
                        reBoton.idResource_int as button_idResource_int,
                        reBoton.title as button_title,
                        reBoton.idResourceType_int as button_idResourceType_int,
                        reBoton.resourceType as button_ResourceType
                FROM resource re
                INNER JOIN (SELECT * FROM resource WHERE idResourceType_int = 49) reFile ON re.idResource_int = reFile.idResource_int
                LEFT  JOIN (SELECT * FROM resource WHERE idResource_int != 49) reBoton ON concat('immersive://action=openActivity&description=Interactive&activity=',reFile.unityTarget,'&userId={{userid}}&username={{username}}&password={{password}}&reto={{reto}}&grado={{grado}}') = reBoton.urlScheme;";
        if($request = $this->select_all('sabana',$sql)){
            return array('status'=>true, 'data'=>$request);
        }
        else{
            return array('status'=>false,'msg'=>'Problemas al cargar catálogo');
        }
    }
}
?>