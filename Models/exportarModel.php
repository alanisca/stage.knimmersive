<?php 

	class exportarModel extends Mysql
	{
        private $idUser_int;//
        private $idChallenge_int;
        private $idResource_int;//
        private $appVersion;
        private $device;
        private $iosDevice;
        private $createTime;
        private $updateTime;

		public function __construct()
		{
			parent::__construct();
		}
        /* UPDATE A RECURSO DE BOTÓN */
        /* Verificar si existen registros por actualizar para el while */
        public function countImmersiveRegister(){
            $sql = "SELECT COUNT(1) as total FROM ImmersiveRegister WHERE idResourceButton IS NULL";
            $request = $this->select('stg', $sql);
            if($request["total"] > 0){
                return array("status"=>true, "data"=>$request);
            }
            else{
                return array("status"=>false);
            }
        }
        /* Seleccionar id de RECURSO y de RETO */
        public function getUpdateImmersiveRegister(){
            $sql = "SELECT idResource_int, idChallenge_int FROM ImmersiveRegister WHERE idResourceButton IS NULL LIMIT 1";
            if($request = $this->select('stg', $sql)){
                return array("status"=>true, "data"=>$request);
            }
            else{
                return array("status"=>false);
            }
        }
        /* Actualizar los recgistros obteniendo el UnityTarget y buscandolo en el reto */
        public function upDateImmersiveRegister(int $idResourceInt, int $idChallengeInt){
            $sql = "SELECT unityTarget FROM Resource WHERE idResource_int = $idResourceInt";
            if($request = $this->select(false, $sql)){
                $sql2 = "SELECT Resource.idResource_int
                        FROM Resource 
                            INNER JOIN ResourceType ON Resource.idResourceType_int = ResourceType.idResourceType_int
                            INNER JOIN Step_Resource ON Resource.idResource_int = Step_Resource.idResource_int
                            INNER JOIN Step ON Step_Resource.idStep_int = Step.idStep_int
                            INNER JOIN Activity ON Step.idActivity_int = Activity.idActivity_int
                            INNER JOIN Session ON Activity.idSession_int = Session.idSession_int
                            INNER JOIN ChallengePathway ON Session.idChallengePathway_int = ChallengePathway.idChallengePathway_int
                            INNER JOIN Challenge ON ChallengePathway.idChallenge_int = Challenge.idChallenge_int
                        WHERE Resource.urlScheme like '%".$request['unityTarget']."%' and Challenge.idChallenge_int = $idChallengeInt LIMIT 1";
                if($request2 = $this->select(false, $sql2)){
                    $queryUpdate = "UPDATE ImmersiveRegister SET idResourceButton = ?
                                    WHERE (idResource_int = $idResourceInt and idChallenge_int = $idChallengeInt)";
                    $requestUpDate = $this->update(true, $queryUpdate, array($request2["idResource_int"]));
                    return array("status"=>true, "data"=>$request2, "upDate"=>$requestUpDate, "msg"=>"Recurso de botón encontrado");
                }
                else{
                    $queryUpdate = "UPDATE ImmersiveRegister SET idResourceButton = ?
                                    WHERE (idResource_int = $idResourceInt and idChallenge_int = $idChallengeInt)";
                    $requestUpDate = $this->update(true, $queryUpdate, array(0));
                    return array("status"=>false, "msg"=>"No se encontró ID Resource de botón");
                }
            }
            else{
                return array("status"=>false, "msg"=>"No se encontró Unity Target");
            }
        }
        /* EXPORTAR A NUEVA TABLA */
        public function selectRegistros(){
            $sql = "SELECT 	kn_registros.id_registro as id_registro
                    FROM kn_registros
                        INNER JOIN kn_usuarios ON kn_registros.id_kn_usuario = kn_usuarios.id_kn_usuario
                        INNER JOIN recursos_imm ON kn_registros.id_recurso = recursos_imm.id_recurso
                        LEFT JOIN Period ON Period.idPeriod_int = kn_registros.idPeriod_int
                    WHERE 
                        latitud IS NULL";
            /* Seleccionar registros actual */
            if($request = $this->select_all('stg',$sql)){ 
                return $request;
            }
            else{
                return array("status" => false, "msg"=>"No se encontraron registros", "SQL"=>$sql);
            }
        }
        public function exportRegistro(int $idRegistro){
            $sql = "SELECT 	kn_usuarios.usuario,
                            kn_pais.id_pais,
                            kn_usuarios.idUser_int as idUser_int,
                            recursos_imm.idResource_int as idResource_int,
                            recursos_imm.unityTarget as unityTarget,
                            kn_registros.version_app as appVersion,
                            kn_registros.dispositivo as device,
                            kn_registros.ios as iosDevice,
                            kn_registros.fecha_registro as createTime,
                            kn_registros.idPeriod_int
                    FROM kn_registros
                        INNER JOIN kn_usuarios ON kn_registros.id_kn_usuario = kn_usuarios.id_kn_usuario
                        INNER JOIN kn_escuelas ON kn_usuarios.id_escuela = kn_escuelas.id_escuela
                        INNER JOIN kn_pais ON kn_escuelas.id_pais = kn_pais.id_pais
                        INNER JOIN recursos_imm ON kn_registros.id_recurso = recursos_imm.id_recurso
                        LEFT JOIN Period ON Period.idPeriod_int = kn_registros.idPeriod_int
                    WHERE id_registro = $idRegistro";
            if($request = $this->select('stg', $sql)){
                $error = null;
                /* Verificar idUser_int */
                if( $request["idUser_int"] !== null && !empty($request["idUser_int"]) ) $this->idUser_int = $request["idUser_int"];
                else{
                    $requestUserID = $this->getIdUserInt($request["usuario"]);
                    if($requestUserID["status"]) $this->idUser_int = $requestUserID["data"];
                    else $error = $error.' idUser_int ';
                }
                /* Verificar idResource_int */
                if( $request["idResource_int"] !== null && !empty($request["idResource_int"]) ) $this->idResource_int = $request["idResource_int"];
                else{
                    $requestResourceInt = $this->getIdResourceInt($request["unityTarget"]);
                    if($requestResourceInt["status"]) $this->idResource_int = $requestResourceInt["data"]["idResource_int"];
                    else $error = $error.' idResource_int ';
                }
                $this->idChallenge_int =  $this->getIdChallenge((int)$this->idResource_int, (int)$request["id_pais"], (string)$request["createTime"], (int)$request["idPeriod_int"]);
                // $this->idChallenge_int = $this->getIdChallenge(191110, 2, "2021-08-05 10:25:22");
                if((bool)$this->idChallenge_int["status"])
                    $this->idChallenge_int = $this->idChallenge_int["data"][0];
                else $error = $error.' idChallenge_int ';
                $this->idPeriodInt = $request["idPeriod_int"];
                $this->appVersion = $request["appVersion"];
                $this->device = $request["device"];
                $this->iosDevice = $request["iosDevice"];
                $this->createTime = $request["createTime"];
                if($error === null){
                    $query_insert =  "INSERT INTO ImmersiveRegister (`idPeriod_int`,`idUser_int`, `idChallenge_int`, `idResource_int`, `appVersion`, `device`, `iosDevice`, `createTime`) VALUES (?,?,?,?,?,?,?,?)";
                    $arrInsert = Array($this->idPeriodInt,
                                        $this->idUser_int,
                                        $this->idChallenge_int["idChallenge_int"],
                                        $this->idResource_int,
                                        $this->appVersion,
                                        $this->device,
                                        $this->iosDevice,
                                        $this->createTime);
                    $request_insert = $this->insert(true ,$query_insert, $arrInsert);

                    $arrUpdate = Array('Exportado');
                }
                else{
                    $arrUpdate = Array('Error:'.$error);
                }
                $queryUpdate = "UPDATE kn_registros SET `latitud` = ? WHERE id_registro = $idRegistro";
                $requestUpDate = $this->update(true, $queryUpdate, $arrUpdate);
                if( !empty($request_insert))
                    return array("status" => true, "msg"=>"Registros encontrados", 
                            "data"=>array("Registro"=>$request,
                                          "idUser_int"=>$this->idUser_int,
                                          "idChallenge_int"=>$this->idChallenge_int["idChallenge_int"],
                                          "idResource_int"=>$this->idResource_int,
                                          "appVersion"=>$this->appVersion,
                                          "device"=>$this->device,
                                          "iosDevice"=>$this->iosDevice,
                                          "createTime"=>$this->createTime,
                                          "Error"=>$error,
                                          "NEWID"=>$request_insert,
                                          "UpDate"=>$requestUpDate));
                else 
                    return array("status" => true, "msg"=>"Registros encontrados", "data"=>array("Registro"=>$request));
            }
        }
        public function getIdUserInt(string $userName){
            $sql = "SELECT idUser_int FROM User WHERE userName = '$userName'";
            echo $sql;
            if($request = $this->select_all(false, $sql)){
                return array("status"=>true,"msg"=>"Usuario encontrado","data"=>$request["idUser_int"]);
            }
            else{
                return array("status"=>false, "msg"=>"Usuario no encontrado");
            }
        }
        public function getIdResourceInt(string $unityTarget){
            $sql = "SELECT idResource_int FROM Resource WHERE unityTarget = $unityTarget";
            if($request = $this->select_all(false, $sql)){
                return array("status"=>true,"msg"=>"Recurso encontrado","data"=>$request);
            }
            else{
                return array("status"=>false, "msg"=>"Recurso no encontrado");
            }
        }
        public function getIdChallenge(int $idResource_int, int $idCountry_int, string $createTime, int $idPeriod_int){
            $sql = "SELECT Challenge.idChallenge_int as idChallenge_int
                    FROM Resource
                        INNER JOIN Challenge_UnityBundle ON Resource.idResource_int = Challenge_UnityBundle.idResource_int
                        INNER JOIN Challenge ON Challenge_UnityBundle.idChallenge_int = Challenge.idChallenge_int
                        INNER JOIN Period ON Challenge.idPeriod_int = Period.idPeriod_int
                    WHERE (Resource.idResource_int = $idResource_int AND Period.idCountry_int = $idCountry_int AND '$createTime' BETWEEN Period.startDate AND Period.endDate) 
                    OR (Period.idPeriod_int = $idPeriod_int AND Resource.idResource_int = $idResource_int AND Period.idCountry_int = $idCountry_int)";
            if($request = $this->select_all(false, $sql)){
                return array("status"=>true,"msg"=>"Challenge encontrado","data"=>$request);
            }
            else{
                return array("status"=>false, "msg"=>"Challenge no encontrado");
            }
        }
        // public function selectChallenge(int $idResource_int){
        //     $sql = "SELECT Resource.title,
        //                     Resource.urlScheme,
        //                     Resource.unityTarget,
        //                     Challenge_UnityBundle.idChallenge_UnityBundle_int,
        //                     Challenge.idChallenge_int,
        //                     Challenge.idGrade_int,
        //                     Challenge.number,
        //                     Challenge.idPeriod_int
        //             FROM Resource
        //             INNER JOIN Challenge_UnityBundle ON Resource.idResource_int = Challenge_UnityBundle.idResource_int
        //             INNER JOIN Challenge ON Challenge_UnityBundle.idChallenge_int = Challenge.idChallenge_int
        //             WHERE Challenge_UnityBundle.idResource_int = $idResource_int";
        //     if($request = $this->select(false, $sql)){
        //         return array("status" => true, "msg"=>"Registros encontrados", "data"=>$request);
        //     }
        //     else{
        //         return array("status" => false, "msg"=>"No se encontraron registros");
        //     }
        // }
	}
 ?>