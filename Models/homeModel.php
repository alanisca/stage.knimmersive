<?php 
	class homeModel extends Mysql
	{
		public function __construct(){
			parent::__construct();
		}
        public function getPeriods(){
            /* seleccionamos los Retos que  */
            $sql = "SELECT idPeriod_int, periodName FROM sabana GROUP BY idPeriod_int";
            if($request = $this->select_all('sabana', $sql)){
                return array("status"=>true, "data"=>$request, "msg"=>"Selecciona periodo para mostrar datos");
            }
            else{
                return array("status"=>false,"msg"=>"No se encontraron periodos ");
            }
        }
       
        public function imm_version_model(){
            $sql = "SELECT version FROM kn_version WHERE status = 1 and id_pais = 1 limit 1";
            if($request = $this->select('stg', $sql)){
                return array("status"=>true,"data"=>$request["version"]);
            }
            else{
                return array("status"=>false, "msg"=>"Sin versión activa para MX");
            }
        }
        public function getResource($idPeriod_int){
            $sql = "SELECT COUNT(DISTINCT resource.idResource_int) as resources
                    FROM resource 
                    INNER JOIN Challenge_UnityBundle ON resource.idResource_int = Challenge_UnityBundle.idResource_int
                    INNER JOIN Challenge ON Challenge_UnityBundle.idChallenge_int = Challenge.idChallenge_int
                    INNER JOIN Period ON Challenge.idPeriod_int = Period.idPeriod_int
                    INNER JOIN Grade ON (Challenge.idGrade_int = Grade.idGrade_int and Grade.status = 'ACT')
                    WHERE Period.idPeriod_int = $idPeriod_int
                    -- GROUP BY 1
                    ORDER BY 1";
            if($request = $this->select('sabana', $sql)){
                return array("status"=>true, "data"=>$request["resources"]);
            }
            else{
                return array("status"=>false, "msg"=>"0");
            }
        }
        public function get_campus($idPeriod_int){
            $sql = "SELECT idPeriod_int, 
                            COUNT(distinct idCampus_int) as totCampus, 
                            COUNT(distinct if(totalResourcesDetonatedByGrade is not null, idCampus_int, null)) as detonedCampus
                    FROM sabana
                    WHERE idPeriod_int = $idPeriod_int
                    GROUP BY 1";
            if($request = $this->select('sabana', $sql)){
                return array("status"=>true, "data"=>$request);
            }
            else{
                return array("status"=>false, "msg"=>"Sin campus para este periodo");
            }
        }
        public function get_users($idPeriod_int){
            $sql = "SELECT COUNT(distinct idUser_int) as usuarios
                    FROM ImmersiveRegister 
                    WHERE idPeriod_int = $idPeriod_int";
            if($request = $this->select('sabana', $sql)){
                return array("status"=>true, "data"=>$request["usuarios"]);
            }
            else{
                return array("status"=>false, "msg"=>"Sin usuarios para este periodo");
            }
        }
        public function get_resourcesByChallenge($idPeriod_int){
            $sql = "SELECT idPeriod_int, 
                            PeriodName, 
                            challengeNumber, 
                            MAX(totalResourcesDetonatedByGrade) as detonated, 
                            MAX(totalResourcesByGrade) as total, 
                            MAX(totalResourcesDetonatedByGrade)/MAX(totalResourcesByGrade) as Percent
                    FROM sabana 
                    WHERE idPeriod_int = $idPeriod_int
                    GROUP BY 1,3
                    ORDER BY idPeriod_int, challengeNumber;";
            if($request = $this->select_all('sabana', $sql)){
                return array("status"=>true, "data"=>$request);
            }
            else{
                return array("status"=>false, "msg"=>"Sin datos para este periodo");
            }
        }
        public function get_resourcesByGrade($idPeriod_int){
            $sql = "SELECT idPeriod_int, 
                            PeriodName, 
                            initialsGrade, 
                            MAX(totalResourcesDetonatedByGrade) as detonated, 
                            MAX(totalResourcesByGrade) as total, 
                            MAX(totalResourcesDetonatedByGrade)/MAX(totalResourcesByGrade) as Percent
                    FROM sabana 
                    WHERE idPeriod_int = $idPeriod_int
                    GROUP BY 1,3
                    ORDER BY idPeriod_int, initialsGrade;";
            if($request = $this->select_all('sabana', $sql)){
                return array("status"=>true, "data"=>$request);
            }
            else{
                return array("status"=>false, "msg"=>"Sin datos para este periodo");
            }
        }
        public function getData_DateModel($idPeriod_int){
            $sql = "SELECT SUBSTR(createTime,1,7) as fecha, COUNT(1) as total FROM ImmersiveRegister WHERE idPeriod_int = $idPeriod_int GROUP BY 1 ORDER BY 1";
            if($request = $this->select_all('stg',$sql)){
                return array("status"=>true, "data"=>$request);
            }
            else{
                return array("status"=>false, "msg"=>"Problemas al encontrar registros por fecha");
            }
        }
        public function getDataModel($idPeriod_int){
            $sql = "SELECT 
                        Period.idPeriod_int,
                        Period.name,
                        Challenge.number as Challenge,
                        Grade.initials,
                        Grade.name,
                        COUNT(DISTINCT Challenge_UnityBundle.idChallenge_UnityBundle_int ) as totalRecursos
                    FROM Grade 
                        INNER JOIN Challenge ON Grade.idGrade_int = Challenge.idGrade_int
                        INNER JOIN Period ON Challenge.idPeriod_int = Period.idPeriod_int
                        INNER JOIN Challenge_UnityBundle ON Challenge.idChallenge_int = Challenge_UnityBundle.idChallenge_int
                        INNER JOIN Resource ON Challenge_UnityBundle.idResource_int = Resource.idResource_int
                    WHERE Period.idPeriod_int = $idPeriod_int
                    GROUP BY 1,2,3,4,5
                    ORDER BY 1,3,4,5";
            if($request = $this->select_all('central',$sql)){
                return array("status"=>true,"data"=>$request,"msg"=>"Datos obtenidos");
            }
            else{
                return array("status"=>false, "msg"=>"Problemas al encontrar datos");
            }
        }
	}
 ?>