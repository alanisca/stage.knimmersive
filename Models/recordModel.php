<?php
class recordModel extends Mysql{
    public function __construct(){
        parent::__construct();
    }
    public function newRecord($arrayRecord){
        if(!isset($arrayRecord['Challenge']) || empty($arrayRecord['Challenge']){
            
        }
        if(!isset($arrayRecord['idUserInt']) || empty($arrayRecord['idUserInt']) ||
            !isset($arrayRecord['idPeriodInt']) || empty($arrayRecord['idPeriodInt']) || 
            !isset($arrayRecord['Challenge']) || empty($arrayRecord['Challenge']) ||
            !isset($arrayRecord['unityTarget']) || empty($arrayRecord['unityTarget']) ){
            return array("username"=>false,"unityTarget"=>false,"newRecord"=>false,"exportData"=>false);
        }
        $this->bitacora(array(
            'userName:'.$arrayRecord['userName'].' | '.
            'unityTarget:'.$arrayRecord['unityTarget'].' | '.
            'accion:'.$arrayRecord['accion'].' | '.
            'date:'.$arrayRecord['date'].' | '.
            'version_app:'.$arrayRecord['version_app'].' | '.
            'dispositivo:'.$arrayRecord['dispositivo'].' | '.
            'ios:'.$arrayRecord['ios'].' | '.
            'school:'.$arrayRecord['school'].' | '.
            'idUserInt:'.$arrayRecord['idUserInt'].' | '.
            'idPeriodInt:'.$arrayRecord['idPeriodInt'].' | '.
            'Period:'.$arrayRecord['Period'].' | '.
            'Campus:'.$arrayRecord['Campus'].' | '.
            'Challenge:'.$arrayRecord['Challenge'].' | '
        , null));

        $idResource_int = $this->IdResource_int($arrayRecord["unityTarget"]);
        if($idResource_int["status"]){
            $idResourceButton_int = $this->IdResourceButton_int($arrayRecord["unityTarget"]);
            if($idResourceButton_int["status"]){
                $idResourceButton_int = $idResourceButton_int["data"]["idResource_int"];
            }
            else{
                $idResourceButton_int = null;
            }
            $sql = "INSERT INTO `ImmersiveRegister` 
                        (`idPeriod_int`,
                        `idUser_int`,
                        `idChallenge_int`,
                        `idResource_int`,
                        `idResourceButton`,
                        `appVersion`,
                        `device`,
                        `iosDevice`,
                        `createTime`
                        )           
                VALUES (?,?,?,?,?,?,?,?,?)";
                $arrInsert = array($arrayRecord['idPeriodInt'],
                                    $arrayRecord['idUserInt'],
                                    $arrayRecord['Challenge'],
                                    $idResource_int["data"]["idResource_int"],
                                    $idResourceButton_int,
                                    $arrayRecord['version_app'],
                                    $arrayRecord['dispositivo'],
                                    $arrayRecord['ios'],
                                    date('Y/m/d H:i:s',
                                    strtotime($arrayRecord["date"]))
                                    );
            if($request = $this->insert('stg',$sql,$arrInsert)){
                $this->saveInShortTabla($sql,$arrInsert);/* Se envían los datos a BD en knImmersive.com */
                return array("username"=>true,"unityTarget"=>$idResource_int["status"],"newRecord"=>true,"exportData"=>true);
            }
            else{
                return array("status"=>false, "msg"=>"No se pudieron guardar los datos");
            }
        }
        else{
            return array("status"=>false, "msg"=>$idResource_int["msg"]);
        }
    }
    public function IdResource_int($unityTarget){
        $sql = "SELECT idResource_int FROM Resource WHERE unityTarget = $unityTarget";
        if($request = $this->select('central',$sql)){
            return array("status"=>true, "data"=>$request);
        }
        else{
            return array("status"=>false, "msg"=>"Sin datos de recurso");
        }
    }
    public function IdResourceButton_int($unityTarget){
        $sql = "SELECT idResource_int, urlScheme FROM resource WHERE urlScheme like '%action=openActivity&description=Interactive&activity=$unityTarget&userId={{userid}}&username={{username}}&password={{password}}&reto={{reto}}&grado={{grado}}'";
        if($request = $this->select('sabana',$sql)){
            return array("status"=>true, "data"=>$request);
        }
        else{
            return array("status"=>false, "msg"=>"Sin datos del boton");
        }
    }
    public function imm_version_model(int $id_pais){
        $sql = "SELECT version FROM kn_version WHERE status = 1 and id_pais = $id_pais limit 1";
        if($request = $this->select('stg', $sql)){
            return array("version"=>$request["version"]);
        }
    }
    public function saveInShortTabla($sqlInsert,$arrSelect){
        /* Busca si el registro ya existe */
        $sql = "SELECT idImmersiveRegister_int
        FROM ImmersiveRegister
        WHERE idPeriod_int = $arrSelect[0] AND 
            idUser_int = $arrSelect[1] AND 
            idChallenge_int = $arrSelect[2] AND 
            idResource_int = $arrSelect[3] AND 
            appVersion = '$arrSelect[5]' AND 
            device = '$arrSelect[6]' AND 
            (iosDevice = '$arrSelect[7]' OR iosDevice is null)";
        if($requestId = $this->select('sabana',$sql)){
            //actualizar update updateTime
            $sql = "UPDATE ImmersiveRegister SET updateTime = ? WHERE idImmersiveRegister_int = $requestId[idImmersiveRegister_int]";
            $arrData = array($arrSelect[8]);
            if($request = $this->update('sabana', $sql, $arrData)){
                return array("status"=>true,"msg"=>"Registro duplicado");
            }
            else{
                return array("status"=>true,"msg"=>"No se pudo actualizar la fecha del nuevo registro");
            }
        }
        else{
            /* Se crea registro */
            if($request = $this->insert('sabana',$sqlInsert,$arrSelect)){
                return array("status"=>true,"msg"=>"Registro nuevo");
            }else{
                return array("status"=>false,"msg"=>"Problemas para generar recurso nuevo");
            }
        }
    }
}
?>
523406