<?php
class getFileImmModel extends Mysql{
    public function __construct(){
        parent::__construct();
    }
    public function getFile(string $fileName){
        $fileName = strClean($fileName);
        $local_file = 'KnImmersiveVideos/'.$fileName;
        if(file_exists($local_file) && $fileName != ""){
            //$this->bitacora(array("IMMERSIVE_APP: Video encontrado: ".$local_file,null));
            return array("status"=>true, "url" => BASE_URL.$local_file);
        }
        else{
            //$this->bitacora(array("IMMERSIVE_APP: Video no encontrado: ".$local_file,null));
            return array("status" => false,"url" => "error 404");
        }
    }
}
?>