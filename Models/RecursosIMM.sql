SELECT Resource.idResource_int,
		Resource.title,
		Challenge.idGrade_int,
		Grade.initials,
        Class.name,
		COUNT(1)
FROM Resource
	INNER JOIN Challenge_UnityBundle ON Resource.idResource_int = Challenge_UnityBundle.idResource_int
	INNER JOIN Challenge ON Challenge_UnityBundle.idChallenge_int = Challenge.idChallenge_int
	INNER JOIN Period ON Challenge.idPeriod_int = Period.idPeriod_int
    INNER JOIN Class ON Period.idPeriod_int = Class.idPeriod_int
	INNER JOIN Grade ON Challenge.idGrade_int = Grade.idGrade_int
WHERE Period.idPeriod_int = ? and Challenge.number = ?
GROUP BY 1,2,3,4,5
ORDER BY 5;

SELECT Resource.idResource_int,
		Resource.title,
		Challenge.number as challenge,
		Grade.initials as initialsGrade
FROM Resource
	INNER JOIN ResourceType ON Resource.idResourceType_int = ResourceType.idResourceType_int
	INNER JOIN Step_Resource ON Resource.idResource_int = Step_Resource.idResource_int
	INNER JOIN Step ON Step_Resource.idStep_int = Step.idStep_int
	INNER JOIN Activity ON Step.idActivity_int = Activity.idActivity_int
	INNER JOIN Session ON Activity.idSession_int = Session.idSession_int
	INNER JOIN ChallengePathway ON Session.idChallengePathway_int = ChallengePathway.idChallengePathway_int
	INNER JOIN Pathway ON ChallengePathway.idPathway_int = Pathway.idPathway_int
	INNER JOIN Challenge ON ChallengePathway.idChallenge_int = Challenge.idChallenge_int
	INNER JOIN Period ON Challenge.idPeriod_int = Period.idPeriod_int
	INNER JOIN Grade ON Challenge.idGrade_int = Grade.idGrade_int
WHERE Resource.idResourceType_int in (58, 229, 230, 231) AND 
	Period.idPeriod_int = ? and Challenge.number = ?;

nombre, ecosistema y campaña

Agreement (Acuerdo)
Campaing/Program