<?php 
class recursosModel extends Mysql {
    public function __construct(){
        parent::__construct();
    }
    public function getResource(int $idPeriod_int, int $challengeNumber, string $initialsGrade){
        $sql = "SELECT
                    Country.`initials` AS 'País',
                    Period.`name` AS 'Periodo',
                    Challenge.`number` AS 'Reto',
                    Grade.`initials` AS 'Grado',
                    Pathway.`name` AS 'Pathway',
                    Language.`initials` AS 'Idioma',
                    Session.`number` AS 'Sesión',
                    TRUNCATE(Activity.`number`/100,0) AS 'No. Actividad',
                    Activity.`name` AS 'Actividad',
                    Step.`number` AS 'Paso',
                    AssociatedResource.`idResource_int` AS 'idResource_int Asociado',
                    AssociatedResource.`title` AS 'Titulo recurso Asociado',
                    AssociatedResource.`description` AS 'Descripción recurso Asociado',
                    AssociatedResource.`urlScheme` AS 'URL Scheme recurso Asociado',
                    AssociatedResourceType.`name` AS 'Tipo recurso Asociado',
                    AssociatedResource.`status` AS 'Estatus recurso Asociado',
                    CONCAT('http://resourcebank.knotion.com/#/set/',ChallengePathway.`idSet_int`,'/',ChallengePathway.`idChallenge_int`,'/',ChallengePathway.`idPathway_int`,'/',ChallengePathway.`idChallengePathway_int`) AS 'Link RB',
                    Step_Resource.updateTime AS 'Ultima fecha de modificicacion'
                FROM Step_Resource
                    JOIN Step ON Step_Resource.`idStep_int` = Step.`idStep_int` AND Step.`status` = 'ACT' AND Step.`isCustom` = 0
                    JOIN Activity ON Step.`idActivity_int` = Activity.`idActivity_int` AND Activity.`status` = 'ACT' AND Activity.`isCustom` = 0
                    JOIN Session ON Activity.`idSession_int` = Session.`idSession_int` AND Session.`status` = 'ACT'
                    JOIN ChallengePathway ON Session.`idChallengePathway_int` = ChallengePathway.`idChallengePathway_int` AND ChallengePathway.`status` = 'ACT'
                    JOIN Pathway ON ChallengePathway.`idPathway_int` = Pathway.`idPathway_int` AND Pathway.`status` = 'ACT' AND Pathway.`isCustom` = 0
                    JOIN Country ON Pathway.`idCountry_int` = Country.`idCountry_int` AND Country.`status` = 'ACT'
                    JOIN Challenge ON ChallengePathway.`idChallenge_int` = Challenge.`idChallenge_int` AND ChallengePathway.`status` = 'ACT'
                    JOIN Grade ON Pathway.`idGrade_int` = Grade.`idGrade_int` AND Grade.`status` = 'ACT'
                    JOIN LevelType ON Grade.`idLevelType_int` = LevelType.`idLevelType_int`
                    JOIN Period ON Challenge.`idPeriod_int` = Period.`idPeriod_int` AND Period.`status` = 'ACT'
                    JOIN Language ON ChallengePathway.`idLanguage_int` = Language.`idLanguage_int` AND Language.`status` = 'ACT'
                    JOIN Resource AssociatedResource ON Step_Resource.`idResource_int` = AssociatedResource.`idResource_int`
                    JOIN ResourceType AssociatedResourceType ON AssociatedResource.`idResourceType_int` = AssociatedResourceType.`idResourceType_int`
                    LEFT JOIN Resource_ResourceTranslationAut ON Step_Resource.`idResourceTranslationAut` = Resource_ResourceTranslationAut.`idResourceTranslationAut`
                WHERE
                    Period.`idPeriod_int` = $idPeriod_int
                    AND AssociatedResourceType.`idResourceType_int` IN (229,230,231,58,50,49)
                    AND Step_Resource.`idResource_int` != 0
                    AND Challenge.`number` = $challengeNumber
                    ".($initialsGrade != "-" ? "AND Grade.`initials` = '".$initialsGrade."'" : "")."
                ORDER BY Period.`name`, Challenge.`number`, Grade.`initials`";
        if($request = $this->select_all('central',$sql)){
            return array('status'=>true, 'data'=>$request, 'sql'=>$sql);
        }
        else{
            return array('status'=>false,'msg'=>'Problemas al cargar recursos vinculados');
        }
    }
}
?>